// pages/info/info.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
//防止连续点击，页面重复跳转
function link(url, that) {
    if (that.data.link == true) {
        wx.navigateTo({
            url: url,
            success: function () {
                that.setData({
                    link: true
                })
            },fail:function(){
                that.setData({
                    link: false
                })
            }
        })
    } else {
        return;
    }
}
// 生日时间戳转换
function Timestamp(timestamp) {
    var date = new Date(timestamp);
    var year = date.getFullYear() + '-';
    var month = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var day = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
    return year + month + day;
}



Page({
    /**
     * 页面的初始数据
     */
    data: {
        userInfo: [],
        gender: '',//性别
        introduce: '',//个人介绍
        nickName: '',//昵称
        avatarUrl: '../../img/defalut.png',//头像
        editName: false,
        nameFouse: false,
        sex: ['女', '男'],
        index: '',
        region: ['上海市', '上海市', '浦东新区'],//默认地址
        date: '',
        birthday: false,
        link: true,
        address: '',
        username: '',
        desc: '',
        mobile: '',
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        end: '',//生日截至今天
    },
    //修改手机号码
    editPhone: function () {
        var that = this;
        // api.Toast(2,'暂不支持修改手机号码！',that);
        wx.navigateTo({
            url: '../editPhone/editPhone?phone=' + that.data.mobile,
        })
    },
    //获取手机号码
    getPhoneNumber: function (e) {
        console.log(e)
        var that = this;

        if (e.detail.iv != null && e.detail.iv.length > 0) {
            wx.request({
                url: baseUrl + '/mobile',
                method: 'PUT',
                data: {
                    iv: e.detail.iv,
                    mobile: e.detail.encryptedData,
                },
                header: {
                    "token": app.globalData.token,
                    'cld.stats.page_entry': app.globalData.scene,
                    'version': app.globalData.version
                },
                success: function (res) {
                    if (res.statusCode == 200) {
                        api.Toast(1, '获取手机号码成功！', that, 1500);
                        var mobile = res.data;
                        app.globalData.userInfo.mobile = mobile;
                        that.setData({
                            mobile: mobile
                        })
                    } else if (res.statusCode == 500) {
                        api.Toast(2, '获取失败，请重新获取！', that, 1500);
                        app.login();
                    }
                }
            })
        }
    },
    //修改性别
    PickerGender: function (e) {
        var that = this;
        var Gender;
        console.log(e.detail.value)
        if (e.detail.value == 0) {
            Gender = 2;
        } else {
            Gender = 1;
        }
        if (Gender != app.globalData.userInfo.gender) {
            wx.request({
                url: baseUrl + '/user',
                method: 'PUT',
                header: {
                    'content-type': 'application/json',
                    'token': app.globalData.token,
                    'cld.stats.page_entry': app.globalData.scene,
                    'version': app.globalData.version
                },
                data: {
                    gender: Gender
                },
                success: function (res) {
                    if (res.statusCode == 200) {
                        api.Toast(1, '修改成功!', that, 1500);
                        that.setData({
                            index: e.detail.value,
                            gender: Gender
                        })
                        app.globalData.userInfo.gender = Gender
                    } else {
                        api.Toast(2, '修改失败!', that, 1500);
                        if (that.data.userInfo.gender == 1) {
                            that.setData({
                                index: 1
                            })
                        } else if (that.data.userInfo.gender == 2) {
                            that.setData({
                                index: 0
                            })
                        } else {
                            that.setData({
                                gender: 0
                            })
                        }
                    }
                },
                fail: function () {
                    that.setData({
                        index: e.detail.value
                    })
                    api.Toast(2, '网络连接错误,修改失败!', that, 1500);
                }
            })
        } else {
            api.Toast(2, '与原性别一致无需修改', that, 1500);
        }
    },
    //修改生日
    PickerDateChange: function (e) {
        var that = this;
        wx.request({
            url: baseUrl + '/user',
            method: 'PUT',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token,
                'cld.stats.page_entry': app.globalData.scene,
                'version': app.globalData.version
            },
            data: {
                birthday: e.detail.value
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    api.Toast(1, '修改成功!', that, 1500);
                    that.setData({
                        date: e.detail.value
                    })
                    app.globalData.userInfo.birthday = e.detail.value
                } else {
                    api.Toast(2, '修改失败!', that, 1500);
                    that.setData({
                        date: that.data.date
                    })
                }
            },
            fail: function () {
                that.setData({
                    date: that.data.date
                })
                api.Toast(2, '网络连接错误,修改失败!', that, 3000);
            }
        })



    },
    //修改地区
    PickerAddress: function (e) {
        var address = e.detail.value[0] + e.detail.value[1] + e.detail.value[2];
        var that = this;
        wx.request({
            url: baseUrl + '/user',
            method: 'PUT',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token,
                'cld.stats.page_entry': app.globalData.scene,
                'version': app.globalData.version
            },
            data: {
                address: address
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    api.Toast(1, '修改成功!', that, 1500);
                    that.setData({
                        address: address
                    })
                    app.globalData.userInfo.address = address
                } else {
                    api.Toast(2, '修改失败!', that, 1500);
                    that.setData({
                        index: that.data.userInfo.address
                    })
                }
            },
            fail: function () {
                that.setData({
                    index: that.data.userInfo.address
                })
                api.Toast(2, '网络连接错误,修改失败!', that, 3000);
            }
        })
    },

    //点击修改头像
    changeAvatarUrl: function () {
        var that = this;
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                var tempFilePaths = res.tempFilePaths
                var size = (res.tempFiles[0].size) / 1024;
                if (size > 1024) {
                    api.Toast(2, '所选图片过大！', that, 1500);
                } else {
                    // console.log(tempFilePaths[0])
                    wx.showLoading({
                        title: '上传中',
                        mask: true
                    })
                    wx.uploadFile({
                        url: baseUrl + '/upload/avatar',
                        filePath: tempFilePaths[0],
                        header: {
                            "token": app.globalData.token,
                            'version': app.globalData.version
                        },
                        method: 'POST',
                        name: '1111.jpg',
                        success: function (res) {
                            if (res.statusCode == 200) {
                                var data = JSON.parse(res.data).url;
                                wx.hideLoading();
                                that.setData({
                                    avatarUrl: data
                                })
                                api.Toast(1, '修改成功！', that, 1500);
                                app.globalData.userInfo.avatar = data
                            }
                        }
                    })
                }

            }
        })
    },
    //点击修改昵称
    editNickName: function () {
        this.setData({
            editName: true,
            nameFouse: true,
        })
    },
    //点击取消修改
    cancelEditnickName: function () {
        this.setData({
            editName: false,
            nameFouse: false,
        })
    },
    //点击背景关闭弹窗
    closeDialog_1: function () {
        this.setData({
            editName: false,
            nameFouse: false,
        })
    },
    //点击修改个人介绍
    editIntroduce: function () {
        var that = this;
        link('../personal/personal', that);
    },
    //点击提交修改用户名
    EditName: function (e) {
        var that = this;
        var nickName = e.detail.value.nickName;
        var testNname = new RegExp("^[A-Za-z0-9\u4e00-\u9fa5]+$");
        var test = testNname.test(nickName);
        if (nickName.length < 2) {
            api.Toast(2, '用户名最小长度为2个字符!', that, 1500);
        } else if (!test) {
            api.Toast(2, '用户名不能包含特殊字符!', that, 1500);
        } else {
            // console.log('修改的用户名为：', nickName);
            // api.Toast(1, '您的用户名修改成功!', that);
            wx.request({
                url: baseUrl + '/user',
                method: 'PUT',
                header: {
                    'content-type': 'application/json',
                    'token': app.globalData.token,
                    'cld.stats.page_entry': app.globalData.scene
                },
                data: {
                    name: nickName
                },
                success: function (res) {
                    // console.log(res);
                    if (res.statusCode == 200) {
                        api.Toast(1, '用户名修改成功!', that, 1500);
                        that.setData({
                            name: nickName,
                            editName: false,
                            username: nickName
                        });
                        app.globalData.userInfo.name = nickName
                    } else {
                        api.Toast(2, '修改失败!', that, 1500);
                        that.setData({
                            name: app.globalData.userInfo.nam,
                            username: app.globalData.userInfo.nam
                        });
                    }
                },
                fail: function () {
                    api.Toast(2, '网络连接错误!', that, 3000);
                }
            })

        }
    },
    //监测输入用户名长度
    checkLength: function (e) {
        var that = this;
        var length = e.detail.value.length;
        if (length >= 8) {
            api.Toast(2, '用户名最大长度为8个字符!', that, 1500);
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var date = new Date();
        var end;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 10) {
            month = '0' + month
        }
        var day = date.getDate();
        if (day < 10) {
            day = '0' + day
        }
        end = year + '-' + month + '-' + day;
        this.setData({
            end: end
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var date = app.globalData.userInfo.birthday;
        if (date) {
            var timestamp = Timestamp(date);
            this.setData({
                date: timestamp,
            })
        } else {
            this.setData({
                date: '',
            })
        }
        // console.log(app.globalData.userInfo.mobile)
        this.setData({
            link: true,
            address: app.globalData.userInfo.address,
            name: app.globalData.userInfo.name,
            username: app.globalData.userInfo.name,
            avatarUrl: app.globalData.userInfo.avatar,
            gender: app.globalData.userInfo.gender,
            mobile: app.globalData.userInfo.mobile,
        });
        var that = this;
        var desc = app.globalData.userInfo.desc;
        if (desc.length > 10) {
            var text = desc.slice(0, 9) + '...';
            this.setData({
                desc: text
            })
        } else {
            this.setData({
                desc: desc
            })
        }
        var Name = app.globalData.userInfo.name;
        if (Name.length > 10) {
            var name = Name.slice(0, 10) + '...';
            this.setData({
                name: name
            })
        } else {
            this.setData({
                name: Name
            })
        }
        var Gender = app.globalData.userInfo.gender
        if (Gender == 1) {
            this.setData({
                index: 1
            })
        } else if (Gender == 2) {
            this.setData({
                index: 0
            })
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

})