// pages/search/search.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
var Auth;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        value: '',
        delet: false,
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        page: 1,
        focus: true,
        search: false,
        searchList: [],
        focueList: [],
        show: true,
        link: true,
        auth: {
            hidden: true
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        Auth = setInterval(() => {
            var auth = wx.getStorageSync('auth');
            if (auth != null && auth.length > 0) {
                clearInterval(Auth);
                var auth = wx.getStorageSync('auth');
                if (auth) {
                    focus(app.globalData.token, that.data.page, that);
                } else {
                    that.setData({
                        auth: true
                    })
                }
            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 10000)
            }
        }, 100)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        this.setData({
            link: true
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {
        clearInterval(Auth);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {
        clearInterval(Auth);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },
    input: function(e) {
        var value = e.detail.value;
        if (value.length > 0) {
            this.setData({
                value: value,
                delet: true
            })
        } else {
            this.setData({
                delet: false
            })
        }
    },
    delet: function() {
        this.setData({
            value: '',
            delet: false
        })
    },
    submit: function() {
        var that = this;
        var value = this.data.value;
        if (this.data.value.length > 0) {
            search(value, that);
        } else {
            api.Toast(2, '请输入搜索内容', that, 1500);
        }
    },
    cancel: function() {
        this.setData({
            focus: true,
            search: false,
            searchList: [],
            value: ''
        })
    },
    userinfo: function(e) {
        var that = this;
        var name = e.detail.userInfo.nickName;
        var Avatar = e.detail.userInfo.avatarUrl;
        wx.request({
            url: baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token
            },
            method: 'PUT',
            data: {
                name: e.detail.userInfo.nickName,
                gender: e.detail.userInfo.gender,
                avatar: Avatar,
                desc: '',
                address: e.detail.userInfo.city
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function (res) {
                            if (res.statusCode == 200) {
                                app.globalData.auth = true;
                                wx.setStorageSync('auth', 'true');
                                // var userInfo = that.data.userInfo;
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                            }
                        },
                        fail: () => {
                            api.Toast(2, '获取数据失败', that, 1500);
                        }
                    })
                } else {
                    api.Toast(2, '获取数据失败', that, 1500);
                }
            },
            fail: function() {
                api.Toast(2, '获取数据失败', that, 1500);
            },
            complete: function() {
                wx.hideLoading();
                var auth = that.data.auth
                auth.hidden = true;
                that.setData({
                    auth: auth
                })
            }
        })
    },
    cancelAuth: function() {
        var that = this;
        var auth = that.data.auth;
        auth.hidden = true;
        that.setData({
            auth: auth
        })
    },
    Detail: function(res) {
        console.log(res);
        var that = this;

        // wx.showToast({
        //     title: '敬请期待',
        //     mask: true,
        //     duration: 1000,
        //     icon: 'none'
        // })
        // var id = res.currentTarget.dataset.id;
        // var url = '../detail/detail?scene=' + id;
        // link(url, that);
    },
    add: function(e) {
        var that = this;
        if (app.globalData.auth) {

            wx.showLoading({
                title: '正在添加...',
                mask: true
            })
            var id = e.currentTarget.dataset.id;
            wx.request({
                url: baseUrl + 'coin/self/add/' + id,
                header: {
                    'content-type': 'application/json',
                    'token': app.globalData.token
                },
                method: 'GET',
                success: res => {
                    if (res.statusCode == 200) {
                        api.Toast(1, '添加成功', that, 1500);
                    } else {
                        api.Toast(2, '不能重复添加', that, 1500);
                    }
                },
                fail: res => {
                    api.Toast(2, '添加失败', that, 1500);
                },
                complete: function() {
                    wx.hideLoading();
                }
            })

        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    }
})

function focus(token, page, that) {
    wx.request({
        url: baseUrl + 'coin/hots',
        header: {
            'content-type': 'application/json',
            'token': token
        },
        method: 'GET',
        success: res => {
            if (res.statusCode == 200) {
                if (res.data.length > 0) {
                    that.setData({
                        focus: true,
                        search: false,
                        focueList: res.data
                    })
                } else {
                    that.setData({
                        focus: false,
                        search: false,
                    })
                }
            } else {
                api.Toast(2, '网络错误！', that, 1500);
            }
        },
        fail: res => {
            api.Toast(2, '网络错误,请求数据失败！', that, 1500);
        },
        complete: function() {
            wx.hideLoading();
        }
    })
}

function search(value, that) {
    wx.showLoading({
        title: '正在搜索中...',
    })
    wx.request({
        url: baseUrl + 'coin/search/' + value,
        header: {
            'content-type': 'application/json',
            'token': app.globalData.token
        },
        method: 'GET',
        success: res => {
            if (res.statusCode == 200) {
                if (res.data.length > 0) {
                    that.setData({
                        focus: false,
                        search: true,
                        searchList: res.data
                    })
                } else {
                    api.Toast(2, '没有搜索到相关内容', that, 1500);
                }

            } else {
                if (res.data.message != null && res.data.message.length > 0) {
                    api.Toast(2, res.data.message, that, 1500);
                } else {
                    api.Toast(2, '没有搜索到相关内容', that, 1500);
                }
                
            }

        },
        fail: res => {
            api.Toast(2, '网络错误，搜索失败！', that, 1500);
        },
        complete: function() {
            wx.hideLoading();
        }
    })
}

function link(url, that) {
    if (that.data.link == true) {
        wx.navigateTo({
            url: url,
            success: function() {
                that.setData({
                    link: false
                })
            },
            fail: function() {
                that.setData({
                    link: false
                })
            }
        })
    } else {
        return false;
    }
}