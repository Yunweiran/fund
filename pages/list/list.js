// pages/list/list.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
var Token;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    link: true,
    type:'',
    item: {
        flag: '',
        msg: '',
        toast: true
    },
    List:[],
    page:1,
    more:true,
    all:false,
    none:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var that = this;
      var type = options.type;
      this.setData({
          type: options.type
      },function(){
          List(app.globalData.token,options.type,that.data.page,that);
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.setData({
      link: true
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      var that =this;
      var page = this.data.page;
      
      if (this.data.more) {
          page++;
          List(app.globalData.token, that.data.type, page, that)
      }else{
          
      }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
        title: '数字金鱼',
      path: '/pages/index/index?scene=' + app.globalData.uid,
      imageUrl: '../../img/logo.png'
    }
  },
  detail: function (e) {
    var that = this;
    // wx.showToast({
    //     title: '敬请期待',
    //     mask: true,
    //     duration: 1000,
    //     icon: 'none'
    // })
    // console.log(e)
    // var url = '../detail/detail?scene=' + e.currentTarget.dataset.id;
    // link(url,that);
  },
  back:function(){
      wx.navigateBack({
          delta: 1
      })
  }
})
function link(url, that) {
  if (that.data.link == true) {
    wx.navigateTo({
      url: url,
      success: function () {
        that.setData({
          link: false
        })
      }
    })
  } else {
    return;
  }
}

function List(token,type,page,that) {
    wx.showLoading({
        title: '加载中...',
    })
    wx.request({
        url: baseUrl + 'coinai',
        header: {
            'content-type': 'application/json',
            'token': token
        },
        method: 'POST',
        data:{
            type:type,
            page:page
        },
        success: function (res) {
            if (res.statusCode == 200) {
               var Data = res.data;
               if(Data !=null && Data.length>0){
                   that.setData({
                       List: that.data.List.concat(Data),
                       page:page
                   })
                   if (Data.length<10){
                       that.setData({
                           more:false,
                           all:true
                       })
                   }else{
                       that.setData({
                           more: true,
                           all: false
                       })
                   }
               }else{
                   that.setData({
                       none: true
                   }) 
               }

            }else{
                api.Toast(2, '获取数据失败', that, 1500);
            }
        },
        fail: function () {
            api.Toast(2,'获取数据失败',that,1500);
        },
        complete: function () {
            wx.hideLoading();
        }
    })
}