// pages/check/check.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
var Auth;
var User;
Page({

    /**
     * 页面的初始数据
    //  */
    data: {
        link: true,
        inputShowed: false,
        inputVal: "",
        List: [],
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        Auth: true,
        userInfo: [],
        avatar:'',
        auth: {
            hidden: true
        },
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        if (options.uid != null && options.uid.length > 0) {
            wx.setStorageSync('shareuid', options.uid);
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    onTabItemTap(item) {
      console.log(item.index)
      console.log(item.pagePath)
      console.log(item.text)
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        that.setData({
            link: true
        });
        var user;
        Auth = setInterval(() => {
            var auth = wx.getStorageSync('auth');
            // var auth ='false'
            if (auth != null && auth.length > 0) {
                clearInterval(Auth);
                var detail = wx.getStorageSync('Detail');
                if (detail) {
                    wx.removeStorageSync('Detail');
                } else {
                    List(app.globalData.token, that);
                }
                if (auth == 'true') {
                    that.setData({
                        Auth: false,
                    }, function () {
                        user = true
                    })

                } else {
                    that.setData({
                        Auth: true
                    })
                }
            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 10000)
            }
        }, 100)

        User = setInterval(() => {
            console.log('User')
            if (app.globalData.userInfo) {
                that.setData({
                    auth: false,
                    avatar: app.globalData.userInfo.avatar,
                    userInfo: app.globalData.userInfo
                },function(){
                    clearInterval(User);
                })  
            }else{
                setTimeout(() => {
                    clearInterval(User);
                }, 10000)
            }

        }, 100)
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearInterval(Auth);
        clearInterval(User);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearInterval(Auth);
        clearInterval(User);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        wx.setStorageSync('Detail', true);
        console.log('/pages/check/check?uid=' + app.globalData.uid)
        return {
            title: '数字金鱼-慧选',
            path: '/pages/check/check?uid=' + app.globalData.uid,
            imageUrl: '../../img/logo.png'
        }
    },
    mine: function () {
        var that = this;
        var url = '../mine/mine';
        link(url, that);
    },
    userinfo: function (e) {
        var that = this;
        var name = e.detail.userInfo.nickName;
        var Avatar = e.detail.userInfo.avatarUrl;
        wx.request({
            url: baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token
            },
            method: 'PUT',
            data: {
                name: e.detail.userInfo.nickName,
                gender: e.detail.userInfo.gender,
                avatar: Avatar,
                desc: '',
                address: e.detail.userInfo.city
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function (res) {
                            if (res.statusCode == 200) {
                                app.globalData.auth = true;
                                wx.setStorageSync('auth', 'true');
                                // var userInfo = that.data.userInfo;
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                                app.globalData.uid = res.data.uid;
                                that.setData({
                                    Auth: false,
                                    avatar: res.data.avatar,
                                    // userInfo: userInfo,
                                })
                            }
                        },
                        fail: () => {
                            api.Toast(2, '获取数据失败', that, 1500);
                        }
                    })
                } else {
                    api.Toast(2, '获取数据失败', that, 1500);
                }
            },
            fail: function () {
                api.Toast(2, '获取数据失败', that, 1500);
            },
            complete: function () {
                wx.hideLoading();
                var auth = that.data.auth
                auth.hidden = true;
                that.setData({
                    auth: auth
                })
            }
        })
    }, 
    cancelAuth: function () {
        var that = this;
        var auth = that.data.auth;
        auth.hidden = true;
        that.setData({
            auth: auth
        })
    },
    more: function (e) {
        var that = this;
        var url = '../list/list?type=' + e.currentTarget.dataset.type;
        wx.setStorageSync('Detail', true);
        link(url, that);
    },
    detail: function (e) {
        var that = this;
        console.log(e)
        // wx.showToast({
        //     title: '敬请期待',
        //     mask: true,
        //     duration: 1000,
        //     icon: 'none'
        // })
        // var url = '../detail/detail?scene=' + e.currentTarget.dataset.id;
        // link(url, that);
    },
    showInput: function () {
        var that = this;
        link('../search/search', that);
    },
    //   hideInput: function () {
    //     this.setData({
    //       inputVal: "",
    //       inputShowed: false
    //     });
    //   },
    //   clearInput: function () {
    //     this.setData({
    //       inputVal: ""
    //     });
    //   },
    //   inputTyping: function (e) {
    //     this.setData({
    //       inputVal: e.detail.value
    //     });
    //   }
})

function List(token, that) {
    wx.showLoading({
        title: '加载中...',
    })
    wx.request({
        url: baseUrl + 'coinais',
        header: {
            'content-type': 'application/json',
            'token': token
        },
        method: 'GET',
        success: function (res) {
            if (res.statusCode == 200) {
                var Data = res.data;
                var type_1 = [];
                var type_5 = [];
                var type_8 = [];
                var type_9 = [];
                var type_10 = [];
                if (Data != null && Data.length > 0) {
                    for (var i = 0; i < Data.length; i++) {
                        if (Data[i].type == 1) {
                            type_1.push(Data[i])
                        } else if (Data[i].type == 5) {
                            type_5.push(Data[i])
                        } else if (Data[i].type == 8) {
                            type_8.push(Data[i])
                        } else if (Data[i].type == 9) {
                            type_9.push(Data[i])
                        } else {
                            type_10.push(Data[i])
                        }
                    }
                    that.setData({
                        List: res.data,
                        type_1: type_1,
                        type_5: type_5,
                        type_8: type_8,
                        type_9: type_9,
                        type_10: type_10,
                    })
                }

            } else {
                if (res.data.message != null && res.data.message.length > 0) {
                    api.Toast(2, res.data.message, that, 1500);
                } else {
                    api.Toast(1, '获取数据失败', that, 1500);
                }
            }
        },
        fail: function () {
            api.Toast(2, '获取数据失败', that, 1500);
        },
        complete: function () {
            wx.hideLoading();
        }
    })
}

function link(url, that) {
    if (that.data.link == true) {
        that.setData({
            link: false
        }, function () {
            wx.navigateTo({
                url: url,
                complete: function () {
                    that.setData({
                        link: true
                    })
                }
            })
        })
    } else {
        return;
    }
}