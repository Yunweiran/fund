// pages/mine/mine.js
var app = getApp();
var baseUrl = getApp().baseUrl;
var ctx;
var Auth;
var CONFIG;
import api from '../../utils/util.js'
//防止连续点击，页面重复跳转
function link(url, that) {
    if (that.data.link == true) {
        that.setData({
            link: false
        }, function () {
            wx.navigateTo({
                url: url,
                complete: function () {
                    that.setData({
                        link: true
                    })
                }
            })
        })
    } else {
        return;
    }
}
Page({
    /**
     * 页面的初始数据
     */
    data: {
        userInfo: [],
        link: true,
        userData: [],
        item: {
            flag: '',
            msg: '',
            toast: true,
        },
        avatar: '../../img/default.png',
        name: '',
        dialog: true,
        previewImg: '',
        code_img: '',
        imgUrl: '',
        ava: '',
        balance: 0,
        auth: {
            hidden: true
        },
        config:''
    },
    //个人资料
    userInfo: function () {
        var that = this;
        link('../info/info', that);
    },
    feedback: function () {
        var that = this;
        link('../feedback/feedback', that);
    },
    linkNotice: function () {
        var that = this;
        link('../notice/notice', that);
    },
    mytopic: function () {
        var that = this;
        link('../mytopic/mytopic', that);
    },
    share: function () {
        wx.showLoading({
            title: '正在生成海报',
        })
        this.code();
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        ctx = wx.createCanvasContext('canvas');
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        var isNotice = wx.getStorageSync('isNotice');
        if (wx.getStorageSync('isNotice') > 0) {
            this.setData({
                showNotice: true
            })
        } else {
            this.setData({
                showNotice: false
            }, function () {
                wx.removeTabBarBadge({
                    index: 2,
                })
            })
        }


        var that = this;
        that.setData({
            link: true
        });
        var user;
        Auth = setInterval(() => {

            // console.log('auth =---0', wx.getStorageSync('auth'))
            var auth = wx.getStorageSync('auth');
            if (auth != null && auth.length > 0) {
                clearInterval(Auth);
                that.notice();
                if (auth == 'true') {
                    
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function (res) {
                            if (res.statusCode == 200) {
                                console.log('user-------------------------:', res.data)
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                                app.globalData.uid = res.data.uid;
                                var Name = res.data.name;
                                if (Name.length > 10) {
                                    var Name = Name.slice(0, 10) + '...';
                                    that.setData({
                                        name: Name
                                    })
                                } else {
                                    that.setData({
                                        name: Name
                                    })
                                }
                                that.setData({
                                    Auth: false,
                                    avatar: res.data.avatar,
                                    userInfo: res.data,
                                    balance: res.data.balance
                                })
                            }
                        },
                        fail: () => {

                        }
                    })
                } else {

                    that.setData({
                        Auth: true
                    }, function () {
                        var auth = that.data.auth;
                        auth.hidden = false
                        that.setData({
                            auth: auth
                        })
                    })
                }
            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 10000)
            }
        }, 100);
        CONFIG = setInterval(() => {
            var config = wx.getStorageSync('config');
            if (config) {
                clearInterval(CONFIG);
                console.log(config)
                that.setData({
                    config: config
                })
            } else {
                setTimeout(() => {
                    clearInterval(CONFIG);
                }, 10000)
            }
        }, 200);

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearInterval(Auth);
        clearInterval(CONFIG);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearInterval(Auth);
        clearInterval(CONFIG);
    },
    onShareAppMessage: function () {
        var that = this;
        var config = wx.getStorageSync('config');
        return {
            title: '点进去一起跟牛人赚钱，获得' + config.inumber + '个糖果',
            path: '/pages/index/index?scene=' + app.globalData.uid,
            imageUrl: '../../img/logo.png'
        }
    },

    config: function () {
        var that = this;
        wx.request({
            url: baseUrl + '/config',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'GET',
            success: res => {
                if (res.statusCode == 200) {
                    that.setData({
                        Number: res.data.number,
                        iNumber: res.data.inumber
                    })
                }
            },
            fail: res => {

            },
            complete: res => {

            }
        })
    },
    notice: function () {
        var that = this;
        wx.request({
            url: baseUrl + 'message/notice',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'GET',
            success: res => {
                if (res.statusCode == 200) {
                    if (res.data.isNotice != 0) {
                        this.setData({
                            showNotice: true
                        })
                    } else {
                        wx.removeTabBarBadge({
                            index: 2,
                        })
                    }
                }
            },
            fail: res => {

            },
            complete: res => {

            }
        })
    },
    onTabItemTap(item) {
        console.log(item.index)
        console.log(item.pagePath)
        console.log(item.text)
    },
    code: function () {
        var that = this;
        wx.request({
            url: baseUrl + 'sharecode',
            method: 'POST',
            header: {
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            data: {
                scene: app.globalData.uid,
                page: 'pages/index/index',
            },
            success: res => {
                if (res.statusCode == 200) {
                    wx.getImageInfo({
                        src: res.data.url,
                        success: function (res) {
                            console.log(res.path)
                            that.setData({
                                code_img: res.path
                            }, function () {
                                that.draw(res.path)
                            })
                        },
                        fail: function () { }
                    })
                } else {
                    api.Toast(2, '获取小程序码失败，请重试', that, 1000);
                }

            },
            fail: function () {
                api.Toast(2, '获取小程序码失败，请重试', that, 1000);
            }
        })
    },
    draw: function (path) {
        var that = this
        ctx.setFillStyle('#ffffff');
        ctx.fillRect(0, 0, 270, 320);
        var avatarurl_width = 50; //绘制的头像宽度
        var avatarurl_heigth = 50; //绘制的头像高度
        var avatarurl_x = 20; //绘制的头像在画布上的位置
        var avatarurl_y = 20; //绘制的头像在画布上的位置
        ctx.save();
        ctx.beginPath(); //开始绘制
        ctx.arc(avatarurl_width / 2 + avatarurl_x, avatarurl_heigth / 2 + avatarurl_y, avatarurl_width / 2, 0, Math.PI * 2, false);
        ctx.clip();
        ctx.drawImage(that.data.avatar, avatarurl_x, avatarurl_y, avatarurl_width, avatarurl_heigth);
        ctx.restore();
        ctx.setFontSize(12);
        ctx.setFillStyle('#999999');
        ctx.fillText('扫码查看,跟牛人一起学挣钱', 65, 280);
        ctx.setFontSize(12);
        ctx.setFillStyle('#999999');
        ctx.fillText('注册即送200币', 90, 300);
        ctx.setFontSize(14);
        ctx.setFillStyle('#989898');
        var title = '你的好友  [ ' + that.data.name + ' ]  邀请你使用数字金鱼';

        var lineWidth = 0;
        var canvasWidth = 320;
        var lastSubStrIndex = 0;
        var initX = 80;
        var initY = 40;
        var lineHeight = 22;
        for (let i = 0; i < title.length; i++) {
            var aaaa = title[i]
            var item = ctx.measureText(aaaa);
            lineWidth += item.width;
            if (lineWidth > canvasWidth - initX * 2) {
                ctx.fillText(title.substring(lastSubStrIndex, i), initX, initY);
                initY += lineHeight;
                lineWidth = 0;
                lastSubStrIndex = i;
            }
            if (i == title.length - 1) {
                ctx.fillText(title.substring(lastSubStrIndex, i + 1), initX, initY);
            }
        }
        ctx.drawImage(path, 60, 100, 150, 150);
        that.setData({
            dialog: false
        })
        ctx.draw(false, function () {
            wx.canvasToTempFilePath({
                x: 0,
                y: 0,
                width: 270,
                height: 320,
                destWidth: 270,
                destHeight: 320,
                canvasId: 'canvas',
                success: function (res) {
                    that.setData({
                        imgUrl: res.tempFilePath
                    })
                },
                fail: function (err) {
                    api.Toast(2, '图片生成失败，请重试！', that, 1000);
                },
                complete: function () {
                    wx.hideLoading();
                }
            })

        })
    },
    //保存图片到本地
    saveImg: function () {
        var that = this;
        wx.saveImageToPhotosAlbum({
            filePath: that.data.imgUrl,
            success: function (res) {
                wx.showModal({
                    title: '成功保存图片',
                    content: '已成功为您保存图片到手机相册，请自行前往朋友圈分享!',
                    showCancel: false,
                    confirmText: '知道了',
                    success: function (res) {
                        if (res.confirm) {
                            that.setData({
                                dialog: true
                            })
                        }
                    }
                })
            },
        })
    },
    close: function () {
        this.setData({
            dialog: true
        })
    },
    userinfo: function (e) {
        var that = this;
        var name = e.detail.userInfo.nickName;
        var Avatar = e.detail.userInfo.avatarUrl;
        wx.request({
            url: baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token
            },
            method: 'PUT',
            data: {
                name: e.detail.userInfo.nickName,
                gender: e.detail.userInfo.gender,
                avatar: Avatar,
                desc: '',
                address: e.detail.userInfo.city
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function (res) {
                            if (res.statusCode == 200) {
                                app.globalData.auth = true;
                                wx.setStorageSync('auth', 'true');
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                                app.globalData.uid = res.data.uid;
                                var Name = res.data.name;
                                if (Name.length > 10) {
                                    var Name = Name.slice(0, 10) + '...';
                                    that.setData({
                                        name: Name
                                    })
                                } else {
                                    that.setData({
                                        name: Name
                                    })
                                }
                                that.setData({
                                    Auth: false,
                                    avatar: res.data.avatar,
                                    userInfo: res.data,
                                    balance: res.data.balance
                                })
                            }
                        },
                        fail: () => {
                            api.Toast(2, '获取数据失败', that, 1500);
                        }
                    })
                } else {
                    api.Toast(2, '获取数据失败', that, 1500);
                }
            },
            fail: function () {
                api.Toast(2, '获取数据失败', that, 1500);
            },
            complete: function () {
                wx.hideLoading();
                var auth = that.data.auth
                auth.hidden = true;
                that.setData({
                    auth: auth
                })
            }
        })
    },
    cancelAuth: function () {
        var that = this;
        var auth = that.data.auth;
        auth.hidden = true;
        that.setData({
            auth: auth
        })
        wx.switchTab({
            url: '../index/index',
        })
    },
})