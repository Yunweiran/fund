// pages/personal/personal.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        num: 30,
        surplus: 30,
        desc: '',
        item: {
            flag: '',
            msg: '',
            toast: true
        }
    },
    Personal: function (e) {
        var that = this
        var length = e.detail.value.text;
        if (length < 2) {
            api.Toast(2, '最少输入2个字符！', that, 1500);
        } else {
            // 提交事件
            var msg = '提交的数据为:' + length


            wx.request({
                url: baseUrl + '/user',
                method: 'PUT',
                header: {
                    'token': app.globalData.token,
                    'cld.stats.page_entry': app.globalData.scene,
                    'version': app.globalData.version
                },
                data: {
                    desc: length
                },
                success: res => {
                    if (res.statusCode == 200) {
                        api.Toast(1, '修改成功！', that, 1500);
                        app.globalData.userInfo.desc = length
                        setTimeout(function () {
                            wx.navigateBack({
                                delta: 1
                            })
                        }, 1500)
                    } else {
                        api.Toast(2, '修改失败！', that, 1500);
                    }
                },
                fail: function () {
                    api.Toast(2, '网络错误！', that, 3000);
                }
            })
        }
    },
    change: function (e) {
        var that = this;
        var Number = e.detail.value.length;//输入字符长度
        var Surplus = this.data.num - Number;//可输入长度
        this.setData({
            surplus: Surplus
        });
        if (Surplus == 0) {
            api.Toast(2, '最多输入30个字符！', that, 1500);
        }
    },
    cancel: function () {
        wx.showModal({
            title: '提示信息',
            content: '是否取消编辑？',
            success: function (res) {
                if (res.confirm) {
                    wx.navigateBack({
                        delta: 1
                    })
                } else if (res.cancel) {
                    // console.log('用户点击取消')
                }
            }

        })
    },
    userInfoHandler: function (e) {
        console.log(e)
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var desc = app.globalData.userInfo.desc;
        if (desc.length > 0) {
            this.setData({
                desc: desc
            })
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    }

})