// pages/notice/notice.js
import api from '../../utils/util.js';
var videoBaseUrl = getApp().videoBaseUrl;
var app = getApp();
var baseUrl = getApp().baseUrl;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        page: 1,
        all: false,
        item: {
            flag: '',
            msg: '',
            toast: true,
            more: true
        },
        all: false,
        notice: [],
        link: true,
        loading: false,
        currentTab: 0,
        index: 0,
        type: 2,
        nav: [{
            name: '评论通知'
        }, {
            name: '邀请通知'
        }],
        commment: true,
        invite: false,
        invite_list: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.list(this.data.type, this.data.page, 1);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.setData({
            link: true
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        wx.showNavigationBarLoading();
        wx.showLoading({
            title: '正在刷新',
            mask: true
        })
        this.list(this.data.type, 1, 0);
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        var that = this;
        var pageSize = this.data.page;
        if (this.data.more) {
            pageSize++;
            this.list(this.data.type, pageSize, 1);
        }
    },
    swichNav: function (e) {
        var that = this;
        var cur = e.target.dataset.current;
        if (this.data.currentTab == cur) {
            return false;
        } else {
            this.setData({
                currentTab: cur,
                page: 1,
                notice: [],
                loading: false
            }, function () {
                if (cur == 0) {
                    that.setData({
                        comment: true,
                        invite: false,
                        type: 2
                    }, function () {
                        that.list(2, 1, 1);
                    })

                } else {
                    that.setData({
                        comment: false,
                        invite: true,
                        type: 3
                    }, function () {
                        that.list(3, 1, 1);
                    })
                }
            })
        }
    },
    release: function () {
        var that = this;
        link('../release/release', that);
    },
    commentPriview: function (e) {
        console.log(e.currentTarget.dataset.img)
        var arry = [];
        arry.push(e.currentTarget.dataset.img);
        wx.previewImage({
            urls: arry,
            current: e.currentTarget.dataset.img,
        })
    },
    linkDetail: function (e) {
        var index = e.currentTarget.dataset.index;
        var mtype = e.currentTarget.dataset.mtype;
        var that = this;
        wx.request({
            url: baseUrl + 'message/notice/read/' + e.currentTarget.dataset.id,
            header: {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'GET',
            success: res => {
                if (res.statusCode == 200) {
                    var data = that.data.notice;
                    var num = wx.getStorageSync('isNotice');
                    num--;
                    if (num > 0) {
                        wx.setTabBarBadge({
                            index: 2,
                            text: num.toString()
                        })
                    } else {
                        num = 0
                        wx.removeTabBarBadge({
                            index: 2,
                        })
                    }
                    // console.log(num.toString()) 
                    app.globalData.isNotice = num;
                    wx.setStorageSync('isNotice', num);
                    data[e.currentTarget.dataset.index].readed = 1;
                    that.setData({
                        notice: data
                    }, function () {
                        if (mtype == 2) {
                            link(e.currentTarget.dataset.url, that);
                        }

                    })

                } else {
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(2, '请求数据失败', that, 1500);
                    }
                }
            },
            fail: function () {
                api.Toast(2, '请求数据失败', that, 1500);
            },
            complete: function () {

            }
        })
    },
    list: function (type, page, flag) {
        if (page > 1) {
            wx.showLoading({
                title: '正在加载...',
                mask: true,
            })
        }

        var that = this;
        wx.request({
            url: baseUrl + 'message/topic/notices',
            header: {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'POST',
            data: {
                page: page,
                type: type
            },
            success: res => {
                if (res.statusCode == 200) {
                    if (res.data != null && res.data.length > 0) {
                        var Data = res.data;
                        for (var i = 0; i < Data.length; i++) {
                            var pics = [];
                            var media = []
                            if (type == 2) {
                                if (Data[i].topic.pics != null && Data[i].topic.pics.length > 0) {
                                    pics = Data[i].topic.pics.split(",");
                                } else {
                                    pics = null
                                }
                                if (Data[i].media != null && Data[i].media.length > 0) {
                                    media = Data[i].media.split(",");
                                } else {
                                    media = null
                                }
                                Data[i].media = media
                                Data[i].topic.pics = pics
                            }
                        }
                        if (flag == 1) {
                            var news = that.data.notice.concat(Data);
                            console.log('news:', news)
                            that.setData({
                                notice: news,
                                page: page
                            })
                        } else {
                            console.log(Data)
                            that.setData({
                                notice: Data,
                                page: page
                            })
                        }

                        if (res.data.length < 10) {
                            that.setData({
                                all: true,
                                more: false
                            })
                        } else {
                            that.setData({
                                all: false,
                                more: true
                            })
                        }
                    } else {
                        that.setData({
                            all: false,
                            more: false
                        })
                    }
                } else {
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(2, '请求数据失败', that, 1500);
                    }

                }

            },
            fail: res => {
                api.Toast(2, '请求数据失败', that, 1500);
            },
            complete: res => {
                that.setData({
                    loading: true
                })
                wx.hideLoading();
                wx.hideNavigationBarLoading();
            }
        })
    },
})

function link(url, that) {
    if (that.data.link == true) {
        that.setData({
            link: false
        }, function () {
            wx.navigateTo({
                url: url,
                complete: function () {
                    that.setData({
                        link: true
                    })
                }
            })
        })
    } else {
        return false;
    }
}