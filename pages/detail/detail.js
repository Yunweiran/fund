// pages/detail/detail.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
var wxCharts = require('../../libs/wxcharts.js');
var Token;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab:0,
    currentTab: 0, //预设当前项的值
    scrollLeft: 0, //tab标题的滚动条位置
    nav: [
      {
        id: 1,
        name: '讨论',
      },
      {
        id: 2,
        name: '项目公告',
      },
      {
        id: 3,
        name: '基本信息'
      },
      {
        id: 4,
        name: '交易所'
      }, {
        id: 5,
        name: '团队'
      }, {
        id: 6,
        name: 'ICO信息'
      }
    ],
    item: {
      flag: '',
      msg: '',
      toast: true
    },
    Detail:[],
    Date:['一月','三月','一年','三年','全部']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     var that = this;
     Token = setInterval(() => {
         var token = wx.getStorageSync('token');
         if (token != null && token.length > 0) {
             clearInterval(Token);
             that.setData({
                 id: options.scene
             }, function () {
                 Detail(token,options.scene, that);
             })

         } else {
             setTimeout(() => {
                 clearInterval(Token);
             }, 6000)
         }
     }, 100)
    var windowWidth = 320;
    try {
      var res = wx.getSystemInfoSync();
      windowWidth = res.windowWidth;
    } catch (e) {
      console.error('getSystemInfoSync failed!');
    }

    var areaChart = new wxCharts({
      canvasId: 'areaCanvas',
      type: 'area',
      categories: ['2016-08', '2016-09', '2016-10', '2016-11', '2016-12', '2017'],
      series: [{
        name: '成交量1',
        data: [70, 40, 65, 100, 34, 18],
        format: function (val) {
          return val.toFixed(2) + '万';
        }
      }, {
        name: '成交量2',
        data: [15, 20, 45, 37, 4, 80],
        format: function (val) {
          return val.toFixed(2) + '万';
        }
      }],
      yAxis: {
        format: function (val) {
          return val + '万';
        }
      },
      width: windowWidth,
      height: 200
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  tab:function(e){
    var cur = e.target.dataset.current;
    if (this.data.tab == cur) {
      return false;
    }
    else {
      this.setData({
        tab: cur
      })
    }
  },
  swichNav: function (e) {
    var cur = e.target.dataset.current;
    if (this.data.currentTaB == cur) {
      return false;
    }
    else {
      this.setData({
        currentTab: cur
      })
    }
    this.checkCor();
  },
  //判断当前滚动超过一屏时，设置tab标题滚动条。
  checkCor: function () {
    var left = (this.data.currentTab - 1) * 93.75;//一个tab标签宽度98px
    if (this.data.currentTab > 1) {
      this.setData({
        scrollLeft: left
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },
})

function Detail(token,id,that){
    wx.showLoading({
        title: '加载中...',
    })
    wx.request({
        url: baseUrl + 'coin/'+id,
        header: {
            'content-type': 'application/json',
            'token': token
        },
        method: 'GET',
        success: function (res) {
            if (res.statusCode == 200) {
                that.setData({
                    Detail:res.data
                })
            }else{
                api.Toast(2, '获取数据失败！', that, 1500);
            }
        },
        fail: function () {
            api.Toast(2,'网络错误，获取数据失败！',that,1500);
        },
        complete: function () {
            wx.hideLoading();
        }
    })
}