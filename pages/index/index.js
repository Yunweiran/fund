var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
var Auth;
var CONFIG;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        people: [],
        tip: [],
        advertise: [],
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        indicatorDots: true,
        autoplay: true,
        interval: 3000,
        duration: 1000,
        cantclick: false,
        auth: {
            hidden: true
        },
        config:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        if (options) {
            var uid = decodeURIComponent(options.scene);
            console.log('index---shareuid', options.scene)
            wx.setStorageSync('shareuid', options.scene);
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        Auth = setInterval(() => {
            var auth = wx.getStorageSync('auth');
            if (auth != null && auth.length > 0) {
                clearInterval(Auth);
                this.people(app.globalData.token);
                this.tip(app.globalData.token);
                this.advertise(app.globalData.token);
            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 6000)
            }
        }, 200);

        CONFIG = setInterval(() => {
            var config = wx.getStorageSync('config');
            if (config != null && config.length > 0) {
                clearInterval(CONFIG);
                console.log(CONFIG)
                that.setData({
                    config: config
                })
            } else {
                setTimeout(() => {
                    clearInterval(CONFIG);
                }, 10000)
            }
        }, 200);


    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {
        clearInterval(Auth);
        clearInterval(CONFIG);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {
        clearInterval(Auth);
        clearInterval(CONFIG);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function(res) {
        wx.setStorageSync('Detail', true);
        var config = wx.getStorageSync('config');
        if (res.from === 'button') {
            console.log('/pages/index/index?scene=' + app.globalData.uid)
            return {
                title: '点进去一起跟牛人赚钱，获得' + config.inumber + '个糖果',
                path: '/pages/index/index?scene=' + app.globalData.uid,
                imageUrl: '../../img/logo.png',
            }
            
        } else {
            console.log('/pages/index/index?scene=' + app.globalData.uid)
            return {
                title: '数字金鱼-首页',
                path: '/pages/index/index?scene=' + app.globalData.uid,
                imageUrl: '../../img/logo.png'
            }
        }
    },
    cancelFocus: function() {
        wx.showToast({
            title: '已经关注过了',
            mask: true,
            duration: 1000,
            icon: 'none'
        })
    },

    people: function(token) {
        var that = this;
        wx.request({
            url: baseUrl + 'celebrity',
            header: {
                'content-type': 'application/json',
                'token': token
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    that.setData({
                        people: res.data
                    })
                }else{
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(1, '获取数据失败', that, 1500);
                    }
                }
            },fail:function(){
                api.Toast(1, '获取数据失败', that, 1500);
            }
        })
    },
    tip: function(token) {
        var that = this;
        wx.request({
            url: baseUrl + 'notice',
            header: {
                'content-type': 'application/json',
                'token': token
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    that.setData({
                        tip: res.data
                    })
                } else {
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(1, '获取数据失败', that, 1500);
                    }
                }
            }, fail: function () {
                api.Toast(1, '获取数据失败', that, 1500);
            }
            
        })
    },
    advertise: function(token) {
        var that = this;
        wx.request({
            url: baseUrl + 'advertise',
            header: {
                'content-type': 'application/json',
                'token': token
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    that.setData({
                        advertise: res.data,
                        autoplay: true
                    })
                }
            }
        })
    },
    focus: function(e) {

        var that = this;
        if (app.globalData.auth) {
            this.setData({
                cantclick: true
            })
            var index = e.currentTarget.dataset.index;
            wx.request({
                url: baseUrl + 'celebrity/attention/' + e.currentTarget.dataset.id,
                header: {
                    'content-type': 'application/json',
                    'token': app.globalData.token
                },
                success: function(res) {
                    if (res.statusCode == 200) {
                        var attention = that.data.people;
                        attention[index].attentions = 1
                        that.setData({
                            people: attention,
                        }, function() {
                            api.Toast(1, '关注成功', that, 1500);
                        })
                    } else {
                        if (res.data.message != null && res.data.message.length > 0) {
                            api.Toast(2, res.data.message, that, 1500);
                        } else {
                            api.Toast(1, '关注失败', that, 1500);
                        }
                        
                    }
                },
                fail: function() {
                    api.Toast(1, '关注失败', that, 1500);
                },
                complete: function() {
                    that.setData({
                        cantclick: false
                    })
                }
            })
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    },
    userinfo: function(e) {
        var that = this;
        var name = e.detail.userInfo.nickName;
        var Avatar = e.detail.userInfo.avatarUrl;
        wx.request({
            url: baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token
            },
            method: 'PUT',
            data: {
                name: e.detail.userInfo.nickName,
                gender: e.detail.userInfo.gender,
                avatar: Avatar,
                desc: '',
                address: e.detail.userInfo.city
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function(res) {
                            if (res.statusCode == 200) {
                                app.globalData.auth = true;
                                wx.setStorageSync('auth', 'true');
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                                app.globalData.uid = res.data.uid;
                                that.setData({
                                    Auth: false,
                                })
                            }
                        },
                        fail: () => {
                            if (res.data.message != null && res.data.message.length > 0) {
                                api.Toast(2, res.data.message, that, 1500);
                            } else {
                                api.Toast(1, '获取数据失败', that, 1500);
                            }
                        }
                    })
                } else {
                    api.Toast(2, '获取数据失败', that, 1500);
                }
            },
            fail: function() {
                api.Toast(2, '获取数据失败', that, 1500);
            },
            complete: function() {
                wx.hideLoading();
                var auth = that.data.auth
                auth.hidden = true;
                that.setData({
                    auth: auth
                })
            }
        })
    },
    cancelAuth: function() {
        var that = this;
        var auth = that.data.auth;
        auth.hidden = true;
        that.setData({
            auth: auth
        })
    },
})