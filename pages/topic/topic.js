import api from '../../utils/util.js';
var app = getApp();
var baseUrl = getApp().baseUrl;
var videoBaseUrl = getApp().videoBaseUrl;
var Auth;
var imgArray = [];
var failArry = [];
var level;
var twolevel;
var aryData;
var ctx;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgurl: [],
        select: false,
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        placeholder: true,
        textarea: '',
        num: 100,
        focus: false,
        dialog: false,
        tid: '', //话题id
        tuid: '', //回复对象id
        suid: '',
        temp: true,
        tempBtn: false,
        TempNum: 50, //点赞数
        TempShow: 0, //是否点赞
        placeholderText: '',
        commentTemp: false,
        loading: true,
        name: '',
        topicDetail: [],
        pid: 0,
        more: true,
        loading: false,
        page: 1,
        replies: '',
        Level: '',
        index: '',
        two_index: '',
        none: false,
        scene: false,
        userid: '',
        link: true,
        alert: true,
        auth: {
            hidden: true
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        this.setData({
            tid: options.scene
        })

        if (options.uid != null && options.uid.length > 0) {
            wx.setStorageSync('shareuid', options.uid);
        }
        Auth = setInterval(() => {
            var auth = wx.getStorageSync('auth');
            if (auth.length > 0) {
                console.log('token:', app.globalData.token)
                clearInterval(Auth);
                var flag = options.flag;
                if (flag) {
                    that.setData({
                        scene: false
                    })
                    if (options.scene != null && options.scene.length > 0) {
                        console.log('token:', app.globalData.token)
                        that.detail(options.scene);
                    } else {
                        api.Toast(2, '获取数据失败,即将跳回社区', that, 1500);
                        setTimeout(() => {
                            wx.reLaunch({
                                url: '../community/community',
                            })
                        }, 1500);
                    }
                } else {
                    that.setData({
                        scene: true
                    })
                    if (options.scene != null && options.scene.length > 0) {
                        that.detail(options.scene);
                    } else {
                        api.Toast(2, '获取数据失败,即将跳回社区', that, 1500);
                        setTimeout(() => {
                            wx.reLaunch({
                                url: '../community/community',
                            })
                        }, 1500);
                    }
                }
            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 15000)
            }
        }, 200);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        ctx = wx.createCanvasContext('canvas')
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.setData({
            link: true
        })
        var that = this;
        console.log('show-focus:', that.data.focus);
        console.log('show-pid:', that.data.pid);
        console.log('show-name:', that.data.name);
        console.log('show-placeholderText:', that.data.placeholderText);
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearInterval(Auth);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearInterval(Auth);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        var that = this;
        if (this.data.more) {
            var page = this.data.page;
            page++;
            this.page(page);
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        var that = this;
        var timestamp = Date.parse(new Date());
        if (that.data.topicDetail.pics != null && that.data.topicDetail.pics.length > 0) {
            return {
                title: that.data.topicDetail.content,
                path: '/pages/topic/topic?scene=' + that.data.topicDetail.id + '&uid=' + app.globalData.uid,
                imageUrl: that.data.topicDetail.pics[0]
            }
        } else {
            return {
                title: that.data.topicDetail.content,
                path: '/pages/topic/topic?scene=' + that.data.topicDetail.id + '&uid=' + app.globalData.uid,
                imageUrl: '../../img/logo.png'
            }
        }
    },
    dialog: function () {
        var that = this;
        if (app.globalData.auth) {
            this.setData({
                dialog: true,
                focus: true,
                pid: 0,
                // name: that.data.topicDetail.ttUser.name,
                // placeholderText: that.data.topicDetail.ttUser.name+'：'
            }, function () {
                console.log('focus:', that.data.focus);
                console.log('pid:', that.data.pid);
                console.log('name:', that.data.name);
                console.log('placeholderText:', that.data.placeholderText);
            })
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    },
    closeDialog: function () {
        this.setData({
            dialog: false,
            focus: false,
            placeholderText: this.data.name + '：'
        })
    },
    // 话题未点赞
    Temp: function (e) {
        var that = this;

        if (app.globalData.auth) {
            this.setData({
                tempBtn: true
            })
            var id = e.currentTarget.dataset.id;
            wx.request({
                url: baseUrl + '/topic/zan/' + id,
                header: {
                    'content-type': 'application/json',
                    'cld.stats.page_entry': app.globalData.scene,
                    'token': app.globalData.token,
                    'version': app.globalData.version
                },
                method: 'GET',
                success: res => {
                    if (res.statusCode == 200) {
                        var num = this.data.TempNum;
                        num++;
                        this.setData({
                            TempShow: 1,
                            TempNum: num
                        })
                    } else {
                        if (res.data.message != null && res.data.message.length > 0) {
                            api.Toast(2, res.data.message, that, 1500);
                        } else {
                            api.Toast(2, '点赞失败', that, 1500);
                        }
                    }
                },
                fail: res => {
                    api.Toast(2, '点赞失败', that, 1500);
                },
                complete: res => {
                    that.setData({
                        tempBtn: false
                    })
                }
            })
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }

    },
    //分享页面放回首页
    back: function () {
        var that = this;
        wx.reLaunch({
            url: '../community/community',
            success: res => {
                that.setData({
                    scene: false
                })
            }
        })
    },
    //话题已经点攒
    temped: function () {
        var that = this;
        wx.showToast({
            title: '你已经点过赞了',
            mask: true,
            icon: 'none',
            duration: 1500
        })
    },
    // 评论未点赞
    commentTemp: function (e) {
        console.log(e)
        var id = e.currentTarget.dataset.id;
        var index = e.currentTarget.dataset.index;
        var that = this;
        if (app.globalData.auth) {
            this.setData({
                commentTemp: true
            })
            wx.request({
                url: baseUrl + '/topic/zan/reply',
                header: {
                    'content-type': 'application/json',
                    'cld.stats.page_entry': app.globalData.scene,
                    'token': app.globalData.token,
                    'version': app.globalData.version
                },
                method: 'POST',
                data: {
                    'tid': that.data.tid,
                    'rid': e.currentTarget.dataset.id
                },
                success: res => {
                    if (res.statusCode == 200) {
                        var data = that.data.replies;
                        data[index].zan = 1;
                        data[index].goodCnt++;
                        that.setData({
                            replies: data
                        })
                    } else {
                        if (res.data.message != null && res.data.message.length > 0) {
                            api.Toast(2, res.data.message, that, 1500);
                        } else {
                            api.Toast(2, '点赞失败', that, 1500);
                        }
                    }
                },
                fail: res => {
                    api.Toast(2, '点赞失败', that, 1500);
                },
                complete: res => {
                    that.setData({
                        commentTemp: false
                    })
                }
            })
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    },
    // 评论已点赞
    commentTemped: function (e) {
        var that = this;
        wx.showToast({
            title: '你已经点过赞了',
            mask: true,
            icon: 'none',
            duration: 1500
        })
    },
    //话题图片预览
    //圈子封面图预览
    preview: function (e) {
        console.log(e.currentTarget.dataset.src)
        var src = e.currentTarget.dataset.src;
        var list = e.currentTarget.dataset.array;
        console.log(list)
        wx.previewImage({
            urls: list,
            current: src,
        })
    },
    //评论图片预览
    commentPriview: function (e) {
        var arry = [];
        arry.push(e.currentTarget.dataset.img);
        wx.previewImage({
            urls: arry,
            current: e.currentTarget.dataset.img,
        })
    },
    //监听评论输入
    input: function (e) {
        var that = this;
        var val = e.detail.value;
        var length = 100 - val.length;
        if (val.length == 0) {
            that.setData({
                placeholder: true,
                num: length,
                textarea: e.detail.value,
            })
        } else {
            if (val.length == 100) {
                wx.showToast({
                    title: '最多输入100字',
                    mask: true,
                    duration: 1500,
                    icon: 'none'
                })
            }
            that.setData({
                textarea: e.detail.value,
                num: length,
                placeholder: false
            })
        }

    },
    //选择图片
    imgUpload: function () {
        var that = this
        if (this.data.imgurl.length > 0) {
            wx.showToast({
                title: '最多选取一张照片',
                icon: 'none',
                duration: 1000,
                mask: true
            })
        } else {
            wx.chooseImage({
                count: 1,
                sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
                success: function (res) {
                    that.setData({
                        imgurl: res.tempFilePaths,
                    })

                },
                fail: function (res) {
                    // fail
                },
                complete: function (res) {
                    // complete
                }
            });
        }

    },
    //清除已选中图片
    clearUrl: function (res) {
        var index = res.currentTarget.dataset.index;

        var imgurl = this.data.imgurl;
        imgurl.splice(index, 1);;
        console.log(imgurl)
        this.setData({
            imgurl: imgurl
        })
        if (imgurl.length < 3) {
            this.setData({
                select: false
            })
        }
    },
    //评论框失去焦点
    blur: function () {
        this.setData({
            placeholderText: this.data.name + '：'
        })
    },
    //提交评论
    submit: function (e) {
        var that = this;
        var str = e.detail.value.content.trim();
        if (str) {
            wx.showLoading({
                title: '正在提交',
                mask: true
            })
            var IMG = that.data.imgurl;
            if (IMG.length > 0) {
                console.log('先上传图片')
                for (var i = 0; i < IMG.length; i++) {
                    that.upload(str, IMG[i]);
                    console.log(IMG[i])
                }

            } else {
                that.form(str, 1);
            }
        } else {
            api.Toast(2, '请输入你要回复的内容！', that, 1500);
        }
    },
    //获取话题详情
    detail: function (id, uid, timestamp) {
        var header;
        if (uid != null && uid.length > 0 && timestamp != null && timestamp.length > 0) {
            header = {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version,
                'shareuid': uid,
                'sharetime': timestamp
            }
        } else {
            header = {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version
            }
        }
        var that = this;
        wx.request({
            url: baseUrl + 'topic/' + id,
            method: 'GET',
            header: header,
            success: res => {
                if (res.statusCode == 200) {
                    var Data = res.data.details;
                    if (Data.pics != null && Data.pics.length > 0) {
                        var media = Data.pics.split(",");
                        Data.pics = media
                    }
                    if (Data.zan) {
                        if (Data.zan == 0) {
                            that.setData({
                                TempShow: 0
                            })
                        } else {
                            that.setData({
                                TempShow: 1
                            })
                        }
                    } else {
                        that.setData({
                            TempShow: 0
                        })
                    }
                    that.setData({
                        tid: Data.id,
                        topicDetail: Data,
                        name: Data.ttUser.name,
                        TempNum: Data.goodCnt,
                        suid: Data.suid,
                        replies: res.data.replies,
                        replyCnt: Data.replyCnt,
                        placeholderText: Data.ttUser.name + '：'
                    })
                    var Reply = res.data.replies;
                    if (Reply != null && Reply.length > 0) {
                        that.setData({
                            none: false
                        })
                        if (Reply.length < 10) {
                            that.setData({
                                all: true,
                                more: false,
                            })
                        } else {
                            that.setData({
                                all: false,
                                more: true,
                            })
                        }
                        console.log()
                    } else {
                        that.setData({
                            more: false,
                            none: true
                        })
                    }
                } else {
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(2, '获取详情失败', that, 1500);
                    }
                }
            },
            fail: res => {
                api.Toast(2, '获取详情失败', that, 1500);
            },
            complete: res => {
                that.setData({
                    loading: true
                })
            },
        });
    },

    reply: function (e) {
        var that = this;
        if (app.globalData.auth) {
            console.log('topic:', e.currentTarget.dataset)
            var url = '../commentPage/commentPage?name=' + e.currentTarget.dataset.name + '&tuid=' + e.currentTarget.dataset.tuid + '&suid=' + e.currentTarget.dataset.suid + '&tid=' + that.data.tid + '&scene=' + e.currentTarget.dataset.id;
            link(url, that);
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    },
    upload: function (content, src) {
        var that = this;

        wx.uploadFile({
            url: baseUrl + 'upload/image',
            filePath: src,
            name: 'file',
            header: {
                'Content-Type': 'multipart/form-data',
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'POST',
            name: '1111.jpg',
            success: res => {

                if (res.statusCode == 200) {
                    imgArray.push(JSON.parse(res.data).url);
                    console.log('imgArray', imgArray)
                    console.log('imgurl', that.data.imgurl)
                    if (imgArray.length == that.data.imgurl.length) {
                        that.form(content, 2);
                    } else {

                    }
                } else {
                    failArry.push(url);
                }
            },
            fail: function () {
                imgArray = [];
                failArry.push(url);
            }
        })

    },

    form: function (content, type) {
        console.log(this.data.index)
        var that = this;
        wx.request({
            url: baseUrl + '/topic/reply',
            header: {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'POST',
            data: {
                "content": content,
                'media': imgArray,
                "type": type,
                "pid": that.data.pid,
                "tuid": that.data.suid,
                'suid': app.globalData.uid,
                "tid": that.data.topicDetail.id,
                'spid': '',
                'time': ''
            },
            success: function (res) {
                // debugger
                if (res.statusCode == 200) {
                    var replyCnt = that.data.replyCnt;
                    replyCnt++;
                    api.Toast(1, '评论成功', that, 1500);
                    failArry = [];
                    imgArray = [];
                    var avatar = app.globalData.userInfo.avatar;
                    var Name = app.globalData.userInfo.name

                    var newReply = [{
                        'active': '1',
                        content: res.data.content,
                        ctime: res.data.ctime,
                        goodCnt: 0,
                        id: res.data.id,
                        media: res.data.media,
                        pid: res.data.pid,
                        replyUser: {
                            avatar: avatar,
                            id: res.data.tuid,
                            name: Name,
                        },
                        tuid: that.data.topicDetail.suid,
                        subReplyList: [],
                        suid: res.data.tuid,
                        tid: res.data.tid,
                        type: type,
                        zan: 0,
                        total: 0
                    }].concat(that.data.replies);;
                    that.setData({
                        bool: false,
                        textarea: '',
                        imgurl: [],
                        replyCnt: replyCnt,
                        select: false,
                        dialog: false,
                        focus: false,
                        num: 100,
                        none: false,
                        placeholder: true,
                        replies: newReply
                        // replies:''
                    }, function () {
                        wx.pageScrollTo({
                            scrollTop: 0,
                            duration: 300
                        })
                    })
                } else {
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(2, '评论失败', that, 1500);
                    }
                }

            },
            fail: res => {
                api.Toast(2, '评论失败', that, 1500);
            },
            complete: res => {
                wx.hideLoading();
            }
        })
    },
    share: function (e) {
        if (app.globalData.auth) {
            wx.showLoading({
                title: '正在生成图片中',
                mask: true,
            })
            var data = e.currentTarget.dataset;
            var that = this;
            wx.getImageInfo({
                src: data.avatar,
                success: function (res) {
                    that.setData({
                        ava: res.path
                    }, function () {

                        that.code(data);
                    })
                },
                fail: function () { }
            })
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    },
    code: function (data) {

        var that = this;
        wx.request({
            url: baseUrl + 'sharecode',
            method: 'POST',
            header: {
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            data: {
                scene: data.id + '&uid=' + app.globalData.uid,
                page: 'pages/community/community',
            },
            success: res => {
                if (res.statusCode == 200) {
                    wx.getImageInfo({
                        src: res.data.url,
                        success: function (res) {
                            console.log(res.path)
                            that.setData({
                                code_img: res.path
                            }, function () {
                                that.draw(res.path, data)
                            })
                        },
                        fail: function () { }
                    })
                } else {
                    api.Toast(2, '获取小程序码失败，请重试', that, 1000);
                }

            },
            fail: function () {
                api.Toast(2, '获取小程序码失败，请重试', that, 1000);
            }
        })
    },
    draw: function (path, data) {
        var that = this
        ctx.setFillStyle('#ffffff');
        ctx.fillRect(0, 0, 270, 360);

        var avatarurl_width = 50; //绘制的头像宽度
        var avatarurl_heigth = 50; //绘制的头像高度
        var avatarurl_x = 20; //绘制的头像在画布上的位置
        var avatarurl_y = 20; //绘制的头像在画布上的位置
        ctx.save();
        ctx.beginPath(); //开始绘制
        ctx.arc(avatarurl_width / 2 + avatarurl_x, avatarurl_heigth / 2 + avatarurl_y, avatarurl_width / 2, 0, Math.PI * 2, false);
        ctx.clip();
        ctx.drawImage(that.data.ava, avatarurl_x, avatarurl_y, avatarurl_width, avatarurl_heigth);
        ctx.restore();
        ctx.setFontSize(16);
        ctx.setFillStyle('#4c84ff');
        ctx.fillText(data.name, 90, 40);
        ctx.setFontSize(14);
        ctx.setFillStyle('#999999');
        ctx.fillText(data.time, 90, 65);


        ctx.setFontSize(12);
        ctx.setFillStyle('#999999');
        ctx.fillText('扫码查看,跟牛人一起学挣钱', 65, 320);

        ctx.setFontSize(12);
        ctx.setFillStyle('#999999');
        ctx.fillText('注册即送200币', 90, 340);

        var title = data.content;
        if (title.length > 30) {
            title = title.substring(0, 30) + '...'
        } else {
            title = title;
        };
        var lineWidth = 0;
        var canvasWidth = 270;
        var lastSubStrIndex = 0;
        var initX = 20;
        var initY = 100;
        var lineHeight = 25;
        ctx.setFontSize(14);
        ctx.setFillStyle('#333333');
        for (let i = 0; i < title.length; i++) {
            var aaaa = title[i]
            var item = ctx.measureText(aaaa);
            lineWidth += item.width;
            if (lineWidth > canvasWidth - initX * 2) {
                ctx.fillText(title.substring(lastSubStrIndex, i), initX, initY);
                initY += lineHeight;
                lineWidth = 0;
                lastSubStrIndex = i;
            }
            if (i == title.length - 1) {
                ctx.fillText(title.substring(lastSubStrIndex, i + 1), initX, initY);
            }
        }
        ctx.drawImage(path, 60, 150, 150, 150);

        that.setData({
            alert: false
        })
        ctx.draw(false, function () {
            wx.canvasToTempFilePath({
                x: 0,
                y: 0,
                width: 270,
                height: 360,
                destWidth: 270,
                destHeight: 360,
                canvasId: 'canvas',
                success: function (res) {
                    that.setData({
                        imgUrl: res.tempFilePath
                    })
                },
                fail: function (err) {
                    api.Toast(2, '图片生成失败，请重试！', that, 1000);
                },
                complete: function () {
                    wx.hideLoading();
                }
            })

        })
    },
    //保存图片到本地
    saveImg: function () {
        var that = this;
        wx.saveImageToPhotosAlbum({
            filePath: that.data.imgUrl,
            success: function (res) {
                wx.showModal({
                    title: '成功保存图片',
                    content: '已成功为您保存图片到手机相册，请自行前往朋友圈分享!',
                    showCancel: false,
                    confirmText: '知道了',
                    success: function (res) {
                        if (res.confirm) {
                            that.setData({
                                alert: true
                            })
                        }
                    }
                })
            },
        })
    },
    closeAlert: function () {
        this.setData({
            alert: true
        })
    },
    page: function (page) {
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        var that = this;
        wx.request({
            url: baseUrl + 'topic/replies',
            header: {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            method: 'POST',
            data: {
                page: page,
                tid: that.data.topicDetail.id,
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    if (res.data.replies != null && res.data.replies.length > 0) {
                        that.setData({
                            replies: that.data.replies.concat(res.data.replies)
                        })
                        if (res.data.length < 10) {
                            that.setData({
                                all: true,
                                more: false
                            })
                        } else {
                            that.setData({
                                all: true,
                                more: false
                            })
                        }
                    } else {
                        that.setData({
                            all: true,
                            more: false
                        })
                    }
                } else {
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(2, '请求数据失败', that, 1500);
                    }
                }
            },
            fail: function () {
                api.Toast(2, '请求数据失败', that, 1500);
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    },
    userinfo: function (e) {
        var that = this;
        console.log(e)
        wx.setStorageSync('userInfo', e.detail.userInfo);
        var name = e.detail.userInfo.nickName;
        wx.showLoading({
            title: '正在提交数据',
        })
        wx.request({
            url: baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token
            },
            method: 'PUT',
            data: {
                name: e.detail.userInfo.nickName,
                gender: e.detail.userInfo.gender,
                avatar: e.detail.userInfo.avatarUrl,
                desc: '',
                address: e.detail.userInfo.city
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function (res) {
                            if (res.statusCode == 200) {
                                app.globalData.auth = true;
                                wx.setStorageSync('auth', 'true');
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                            }
                        },
                        fail: () => {
                            api.Toast(2, '获取数据失败', that, 1500);
                        }
                    })
                } else {
                    api.Toast(2, '获取数据失败', that, 1500);
                }
            },
            fail: function () {
                api.Toast(2, '获取数据失败', that, 1500);
            },
            complete: function () {
                wx.hideLoading();
                var auth = that.data.auth
                auth.hidden = true;
                that.setData({
                    auth: auth
                })
            }
        })
    },
    cancelAuth: function () {
        var that = this;
        var auth = that.data.auth;
        auth.hidden = true;
        that.setData({
            auth: auth
        })
    },
})

function timestamp(timestamp) {
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y + M + D + h + m + s;
}

function link(url, that) {
    if (that.data.link == true) {
        wx.navigateTo({
            url: url,
            success: function () {
                that.setData({
                    link: true
                })
            },
            fail: function () {
                that.setData({
                    link: false
                })
            },
            complete: function () {
                that.setData({
                    link: true
                })
            }
        })
    } else {
        return;
    }
}