// pages/release/release.js
var amapFile = require('../../libs/amap-wx.js');
var config = require('../../libs/config.js');
import api from '../../utils/util.js';

var videoBaseUrl = getApp().videoBaseUrl;
var app = getApp();
var baseUrl = getApp().baseUrl;
var imgArray = [];
var result = 0;
var failArry = []
Page({

    /**
     * 页面的初始数据
     */
    data: {
        latitude: '',
        longitude: '',
        address: '',
        gps: false,
        close: false,
        imgurl: [],
        select: false,
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        textarea: '',
        num: 500
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        var token = app.globalData.token;
        GPS(that);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    input: function (e) {
        var that = this;
        var val = e.detail.value;
        var length = 500 - val.length;
        if (val.length == 0) {
            that.setData({
                num: length,
                textarea: e.detail.value,
            })
        } else {
            that.setData({
                textarea: e.detail.value,
                num: length,
            })
            if (val.length == 500) {
                wx.showToast({
                    title: '最多可输入500字',
                    icon: 'none',
                    mask: true,
                    duration: 1500
                })
            }
        }

    },
    location: function () {
        var that = this;
        var key = config.Config.key;
        var gps = wx.getStorageSync('gps');
        if (gps) {
            GPS(that);
        } else {
            wx.openSetting({
                success: function (res) {
                    if (res.authSetting["scope.userLocation"]) {
                        GPS(that);
                    } else {
                        that.setData({
                            gps: false,
                            close: false,
                        })
                        wx.setStorageSync('gps', false)
                    }
                }
            })
        }

    },
    close: function () {
        var that = this;
        that.setData({
            gps: false,
            close: false,
            address: ''
        })
    },
    again: function () {
        var that = this;
        that.setData({
            bool: false
        })
    },
    imgUpload: function () {
        var that = this
        var num = 3 - this.data.imgurl.length
        wx.chooseImage({
            count: num, // 默认2
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                var array = [];
                console.log(typeof that.data.imgurl)
                var array = that.data.imgurl.concat(res.tempFilePaths);
                that.setData({
                    imgurl: array
                })

                if (array.length < 3) {
                    that.setData({
                        select: false
                    })
                } else if (array.length == 3) {
                    that.setData({
                        select: true
                    })
                } else {

                }

            }, fail: function (res) {
                // fail
            },
            complete: function (res) {
                // complete
            }
        });

    },
    //清除已选中图片
    clearUrl: function (res) {
        var index = res.currentTarget.dataset.index;
        var imgurl = this.data.imgurl;
        imgurl.splice(index, 1);;
        console.log(imgurl)
        this.setData({
            imgurl: imgurl
        })
        if (imgurl.length < 3) {
            this.setData({
                select: false
            })
        }
    },
    preview: function (e) {
        var that = this;
        var src = e.currentTarget.dataset.src;
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: that.data.imgurl // 需要预览的图片http链接列表
        })
    },
    submit: function (e) {

        var that = this;
        var str = e.detail.value.content.trim();
        if (str) {
            wx.showLoading({
                title: '正在提交',
                mask: true
            })
            var IMG = that.data.imgurl;
            if (IMG.length > 0) {
                console.log('先上传图片')
                for (var i = 0; i < IMG.length; i++) {
                    upload(str, IMG[i], that, 3);
                    console.log(IMG[i])
                }

            } else {
                form(str, that, 1);
            }
        } else {
            api.Toast(2, '请输入你要发布的内容！', that, 1500);
        }
    }

})

function GPS(that) {
    var key = config.Config.key;
    var myAmapFun = new amapFile.AMapWX({ key: key });
    // var gps = wx.getStorageSync('gps');
    // console.log(gps)
    wx.getLocation({
        type: 'gcj02',
        success: function (res) {
            myAmapFun.getRegeo({
                success: function (data) {
                    wx.setStorageSync('gps', true);
                    // console.log(data)
                    var address = data[0].regeocodeData.addressComponent.province + data[0].regeocodeData.addressComponent.city + data[0].regeocodeData.addressComponent.district;
                    if (address.length > 15) {
                        var Address = address.slice(0, 12) + '...';
                    } else {
                        var Address = address;
                    }
                    that.setData({
                        gps: true,
                        close: true,
                        address: Address
                    })
                },
                fail: function (info) {
                    //失败回调
                    that.setData({
                        gps: false,
                        close: false,
                    })
                }
            })
        },
        fail: function () {
            wx.setStorageSync('gps', false)
        }
    })
}

function upload(content, src, that, type) {
    wx.uploadFile({
        url: baseUrl + 'upload/image',
        filePath: src,
        name: 'file',
        header: {
            'Content-Type': 'multipart/form-data',
            'token': app.globalData.token,
            'version': app.globalData.version
        },
        method: 'POST',
        name: '1111.jpg',
        success: res => {
            if (res.statusCode == 200) {
                imgArray.push(JSON.parse(res.data).url);
                console.log('imgArray', imgArray)
                console.log('imgurl', that.data.imgurl)
                if (imgArray.length == that.data.imgurl.length) {
                    form(content, that, type);
                }
            } else {
                wx.showToast({
                    title: '发布失败,请重试',
                    mask: true,
                    duration: 1000,
                    icon: 'none'
                })
            }
        },
        fail: function () {
            imgArray = [];
            failArry.push(url);
            wx.showToast({
                title: '发布失败,请重试',
                mask: true,
                duration: 1000,
                icon: 'none'
            })
        }
    })
}

function form(content, that, type) {
    console.log(app.globalData.scene);
    wx.request({
        url: baseUrl + '/topic/submit',
        header: {
            'content-type': 'application/json',
            'cld.stats.page_entry': app.globalData.scene,
            'token': app.globalData.token,
            'version': app.globalData.version
        },
        method: 'POST',
        data: {
            "content": content,
            'pics': imgArray,
            'address': that.data.address,
        },
        success: function (res) {
            if (res.statusCode == 200) {
                
                failArry = [];
                imgArray = [];
                that.setData({
                    bool: false,
                    textarea: '',
                    imgurl: [],
                    select: false,
                }, function () {
                    api.Toast(1, res.data.msg, that, 1500);
                    setTimeout(()=>{
                        wx.switchTab({
                            url: '../community/community',
                            success: function (e) {
                                var page = getCurrentPages().pop();
                                if (page == undefined || page == null) return;
                                page.onLoad();
                            }
                        })
                    },1500)
                    
                })
            } else {
                if (res.data.message != null && res.data.message.length > 0) {
                    api.Toast(2, res.data.message, that, 1500);
                } else {
                    api.Toast(2, '发布失败', that, 1500);
                }
            }

        },
        fail: res => {
            api.Toast(2, '发布失败', that, 1500);
        },
        complete: res => {
            wx.hideLoading();
        }
    })
}