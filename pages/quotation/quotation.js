// pages/quotation/quotation.js
var app = getApp();
var baseUrl = getApp().baseUrl;
import api from '../../utils/util.js';
var Token;
var User;
var Auth;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        tab: 0,
        tag: ['行情','自选'],
        add: false,
        check: false,
        inputShowed: false,
        inputVal: "",
        page: 1,
        List: [],
        Data: false,
        none: false,
        more: true,
        all: false,
        link: true,
        item: {
            flag: '',
            msg: '',
            toast: true
        },
        userInfo: [],
        avatar: '',
        auth: {
            hidden: true
        },
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        if (options.uid != null && options.uid.length > 0) {
            wx.setStorageSync('shareuid', options.uid);
        }

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this;
        this.setData({
            link: true,
        })
        var user;
        Auth = setInterval(() => {
            var auth = wx.getStorageSync('auth');
            if (auth != null && auth.length > 0) {
                clearInterval(Auth);
                var auth = wx.getStorageSync('auth');
                var detail = wx.getStorageSync('Detail');
                if (detail) {
                    wx.removeStorageSync('Detail');
                } else {
                    if (that.data.tab == 1) {
                        this.setData({
                            List: []
                        }, function() {
                            selfList(app.globalData.token, that.data.page, that)
                        })
                    } else {
                        this.setData({
                            List: []
                        }, function() {
                            List(app.globalData.token, that.data.page, that)
                        })
                    }

                    if (auth == 'true') {
                        that.setData({
                            Auth: false,
                        }, function() {
                            user = true
                        })
                    } else {
                        that.setData({
                            auth: true
                        })
                    }
                }


            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 10000)
            }
        }, 100)

        User = setInterval(() => {
            if (app.globalData.userInfo) {
                that.setData({
                    auth: false,
                    avatar: app.globalData.userInfo.avatar,
                    userInfo: app.globalData.userInfo
                }, function() {
                    clearInterval(User);
                })
            } else {
                setTimeout(() => {
                    clearInterval(User);
                }, 10000)
            }

        }, 100)
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {
        clearInterval(Auth);
        clearInterval(User);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {
        clearInterval(Auth);
        clearInterval(User);
    },
    onTabItemTap(item) {
        console.log(item.index)
        console.log(item.pagePath)
        console.log(item.text)
    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        var that = this;
        if (that.data.more) {
            var page = that.data.page;
            page++;
            if (that.data.tab == 1) {
                selfList(app.globalData.token, page, that)
            } else {
                List(app.globalData.token, page, that)
            }
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {
        wx.setStorageSync('Detail', true);
        return {
            title: '数字金鱼-行情',
            path: '/pages/quotation/quotation?uid=' + app.globalData.uid,
            imageUrl: '../../img/logo.png'
        }
    },
    userinfo: function(e) {
        var that = this;
        var name = e.detail.userInfo.nickName;
        var avatar = e.detail.userInfo.avatarUrl;
        debugger
        wx.request({
            url: baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': app.globalData.token
            },
            method: 'PUT',
            data: {
                name: e.detail.userInfo.nickName,
                gender: e.detail.userInfo.gender,
                avatar: avatar,
                desc: '',
                address: e.detail.userInfo.city
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    wx.request({
                        url: baseUrl + 'user',
                        header: {
                            'content-type': 'application/json',
                            'token': app.globalData.token
                        },
                        method: 'GET',
                        success: function(res) {
                            if (res.statusCode == 200) {
                                app.globalData.auth = true;
                                wx.setStorageSync('auth', 'true');
                                // var userInfo = that.data.userInfo;
                                wx.setStorageSync('userInfo', res.data);
                                app.globalData.userInfo = res.data;
                                app.globalData.uid = res.data.uid;
                                that.setData({
                                    Auth: false,
                                    avatar: res.data.avatar,
                                    // userInfo: userInfo,
                                })
                            }
                        },
                        fail: () => {
                            api.Toast(2, '获取数据失败', that, 1500);
                        }
                    })
                } else {
                    api.Toast(2, '获取数据失败', that, 1500);
                }
            },
            fail: function() {
                api.Toast(2, '获取数据失败', that, 1500);
            },
            complete: function() {
                wx.hideLoading();
            }
        })
    },
    mine: function() {
        var that = this;
        var url = '../mine/mine';
        link(url, that);
    },
    tab: function(e) {
        var that = this;
        var cur = e.target.dataset.current;
        if (this.data.tab == cur) {
            return false;
        } else {
            this.setData({
                tab: cur,
                add: false,
                none: false,
                check: false,
                Data: false,
                more: true,
                all: false,
                page: 1,
                List: [],
            })
            if (cur == 0) {
                List(app.globalData.token, 1, that)
            } else {
                selfList(app.globalData.token, 1, that)
            }
        }
    },
    showInput: function() {
        var that = this;
        link('../search/search', that);

        // this.setData({
        //     inputShowed: true
        // });
    },
    hideInput: function() {
        this.setData({
            inputVal: "",
            inputShowed: false
        });
    },
    clearInput: function() {
        this.setData({
            inputVal: ""
        });
    },
    inputTyping: function(e) {
        this.setData({
            inputVal: e.detail.value
        });
    },
    Detail: function(res) {
        var that = this;
        wx.setStorageSync('Detail', true);
        // wx.showToast({
        //     title: '敬请期待',
        //     mask: true,
        //     duration: 1000,
        //     icon: 'none'
        // })
        // var id = res.currentTarget.dataset.id;
        // var url = '../detail/detail?scene=' + id;
        // link(url, that);
    },
    add: function() {
        var that = this;
        link('../search/search', that);
    }
})

function List(token, page, that) {
    console.log("List")
    wx.showLoading({
        title: '加载中...',
    })
    wx.request({
        url: baseUrl + 'coins',
        header: {
            'content-type': 'application/json',
            'token': token
        },
        method: 'POST',
        data: {
            page: page,
        },
        success: function(res) {
            if (res.statusCode == 200) {
                that.setData({
                    page: page
                })
                var data = that.data.List.concat(res.data);
                that.setData({
                    check: true,
                    List: data
                })
                if (res.data.length < 10) {
                    that.setData({
                        all: true,
                        more: false
                    })
                } else {
                    that.setData({
                        all: false,
                        more: true
                    })
                }
            } else {
                if (res.data.message != null && res.data.message.length > 0) {
                    api.Toast(2, res.data.message, that, 1500);
                } else {
                    api.Toast(1, '获取数据失败', that, 1500);
                }
            }
        },
        fail: function() {
            api.Toast(2, '获取数据失败', that, 1500);
        },
        complete: function() {
            wx.hideLoading();
        }
    })
}

function selfList(token, page, that) {
    wx.showLoading({
        title: '加载中...',
    })
    wx.request({
        url: baseUrl + 'coins/self',
        header: {
            'content-type': 'application/json',
            'token': token
        },
        method: 'POST',
        data: {
            page: page,
        },
        success: function(res) {
            if (res.statusCode == 200) {
                var Data = res.data;
                that.setData({
                    List: that.data.List.concat(Data),
                    page: page,
                    add: true
                })
                //判断第一页是否有数据显示添加自选
                if (page == 1) {
                    if (Data.length == 0) {
                        that.setData({
                            check: false,
                            none: true,
                            more: false
                        })
                    } else {
                        if (Data.length < 10) {
                            that.setData({
                                check: true,
                                none: false,
                                more: false
                            })
                        } else {
                            that.setData({
                                check: true,
                                none: false,
                                more: true
                            })
                        }
                    }
                } else { //判断其他页是否开启下拉加载
                    if (Data.length < 10) {
                        that.setData({
                            more: false
                        })
                    } else {
                        that.setData({
                            more: true
                        })
                    }
                }

            } else {
                if (res.data.message != null && res.data.message.length > 0) {
                    api.Toast(2, res.data.message, that, 1500);
                } else {
                    api.Toast(1, '获取数据失败', that, 1500);
                }
            }
        },
        fail: function() {
            api.Toast(2, '获取数据失败', that, 1500);
        },
        complete: function() {
            wx.hideLoading();
        }
    })
}

function link(url, that) {
    if (that.data.link == true) {
        that.setData({
            link: false
        }, function() {
            wx.navigateTo({
                url: url,
                complete: function() {
                    that.setData({
                        link: true
                    })
                }
            })
        })
    } else {
        return false;
    }
}