// pages/commentPage/commentPage.js
import api from '../../utils/util.js';
var app = getApp();
var baseUrl = getApp().baseUrl;
var imgArray = [];
var failArry = [];
var level;
var twolevel;
var aryData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pid: '',
    tid: '',
    tuid: '',
    rid: '',
    spid: '',
    page: 1,
    item: {
      flag: '',
      msg: '',
      toast: true
    },
    placeholder: true,
    textarea: '',
    num: 100,
    focus: false,
    dialog: true,
    none: false,
    detail: '',
    replies: [],
    TempNum: '',
    TempShow: 0,
    placeholderText: '',
    imgurl: [],
    name: '',
    replies: [],
    goodCnt: '',
    level: 0,
    more: true,
    all: false,
    auth: {
      hidden: true
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    console.log('topicDetail:', options);
    // debugger
    this.setData({
      tid: options.tid,
      pid: options.scene,
      tuid: options.suid,
      rid: options.tuid,
      name: options.name,
      spid: options.scene,
      placeholderText: options.name + '：'
    }, function() {
      that.list(options.tid, that.data.page, options.scene);
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    if (this.data.more) {
      var page = this.data.page;
      page++;
      this.list(that.data.tid, page, that.data.spid);
    }
  },
  reply: function(e) {
    var that = this;
    var name = e.currentTarget.dataset.name + ':';
    var pid = e.currentTarget.dataset.pid;
    var index = e.currentTarget.dataset.index;
    console.log(e.currentTarget.dataset)
    if (app.globalData.auth) {
      this.setData({
        dialog: false,
        tuid: e.currentTarget.dataset.suid,
        focus: true,
        pid: e.currentTarget.dataset.pid,
        placeholderText: name,
        name: e.currentTarget.dataset.name,
      })
    } else {
      var auth = that.data.auth;
      auth.hidden = false
      that.setData({
        auth: auth
      })
    }
  },
  dialog: function() {
    var that = this;
    if (app.globalData.auth) {
      this.setData({
        dialog: false,
        focus: true,
        pid: that.data.pid,
      }, function() {
        console.log('pid:', that.data.pid);
        console.log('name:', that.data.name);
      })
    } else {
      var auth = that.data.auth;
      auth.hidden = false
      that.setData({
        auth: auth
      })
    }
  },
  closeDialog: function() {
    this.setData({
      dialog: true,
      focus: false,
      placeholderText: this.data.name + '：'
    })
  },
  // 话题未点赞
  Temp: function(e) {
    console.log(e)
    var that = this;
    var id = e.currentTarget.dataset.id;
    var that = this;
    if (app.globalData.auth) {
      this.setData({
        commentTemp: true
      })
      wx.request({
        url: baseUrl + 'topic/zan/reply',
        header: {
          'content-type': 'application/json',
          'cld.stats.page_entry': app.globalData.scene,
          'token': app.globalData.token,
          'version': app.globalData.version
        },
        method: 'POST',
        data: {
          'tid': that.data.tid,
          'rid': e.currentTarget.dataset.id
        },
        success: res => {
          if (res.statusCode == 200) {
            var goodCnt = that.data.goodCnt;
            goodCnt++;
            that.setData({
              TempShow: 1,
              goodCnt: goodCnt
            })
          } else {
            if (res.data.message != null && res.data.message.length > 0) {
              api.Toast(2, res.data.message, that, 1500);
            } else {
              api.Toast(2, '点赞失败', that, 1500);
            }
          }
        },
        fail: res => {
          api.Toast(2, '点赞失败', that, 1500);
        },
        complete: res => {
          that.setData({
            commentTemp: false
          })
        }
      })
    } else {
      var auth = that.data.auth;
      auth.hidden = false
      that.setData({
        auth: auth
      })
    }

  },
  temped: function() {
    var that = this;
    wx.showToast({
      title: '你已经点过赞了',
      mask: true,
      icon: 'none',
      duration: 1500
    })
  },
  commentTemp: function(e) {
    console.log(e)
    var id = e.currentTarget.dataset.id;
    var index = e.currentTarget.dataset.index;
    var that = this;
    if (app.globalData.auth) {
      this.setData({
        commentTemp: true
      })
      wx.request({
        url: baseUrl + 'topic/zan/reply',
        header: {
          'content-type': 'application/json',
          'cld.stats.page_entry': app.globalData.scene,
          'token': app.globalData.token,
          'version': app.globalData.version
        },
        method: 'POST',
        data: {
          'tid': that.data.tid,
          'rid': e.currentTarget.dataset.id
        },
        success: res => {
          if (res.statusCode == 200) {
            var data = that.data.replies;
            data[index].zan = 1;
            data[index].goodCnt++;
            that.setData({
              replies: data
            })
          } else {
            if (res.data.message != null && res.data.message.length > 0) {
              api.Toast(2, res.data.message, that, 1500);
            } else {
              api.Toast(2, '点赞失败', that, 1500);
            }
          }
        },
        fail: res => {
          api.Toast(2, '点赞失败', that, 1500);
        },
        complete: res => {
          that.setData({
            commentTemp: false
          })
        }
      })
    } else {
      var auth = that.data.auth;
      auth.hidden = false
      that.setData({
        auth: auth
      })
    }
  },
  // 评论已点赞
  commentTemped: function(e) {
    var that = this;
    wx.showToast({
      title: '你已经点过赞了',
      mask: true,
      icon: 'none',
      duration: 1500
    })
  },
  //话题图片预览
  //圈子封面图预览
  preview: function(e) {
    console.log(e.currentTarget.dataset.src)
    var arry = [];
    arry.push(e.currentTarget.dataset.src);
    wx.previewImage({
      urls: arry,
      current: src,
    })
  },
  //评论图片预览
  commentPriview: function(e) {
    var arry = [];
    arry.push(e.currentTarget.dataset.img);
    wx.previewImage({
      urls: arry,
      current: e.currentTarget.dataset.img,
    })
  },
  //监听评论输入
  input: function(e) {
    var that = this;
    var val = e.detail.value;
    var length = 100 - val.length;
    if (val.length == 0) {
      that.setData({
        placeholder: true,
        num: length,
        textarea: e.detail.value,
      })
    } else {
      if (val.length == 100) {
        wx.showToast({
          title: '最多输入100字',
          mask: true,
          duration: 1500,
          icon: 'none'
        })
      }
      that.setData({
        textarea: e.detail.value,
        num: length,
        placeholder: false
      })
    }

  },
  //选择图片
  imgUpload: function() {
    var that = this
    if (this.data.imgurl.length > 0) {
      wx.showToast({
        title: '最多选取一张照片',
        icon: 'none',
        duration: 1000,
        mask: true
      })
    } else {
      wx.chooseImage({
        count: 1,
        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function(res) {
          that.setData({
            imgurl: res.tempFilePaths,
          })

        },
        fail: function(res) {
          // fail
        },
        complete: function(res) {
          // complete
        }
      });
    }

  },
  //清除已选中图片
  clearUrl: function(res) {
    var index = res.currentTarget.dataset.index;

    var imgurl = this.data.imgurl;
    imgurl.splice(index, 1);;
    console.log(imgurl)
    this.setData({
      imgurl: imgurl
    })
    if (imgurl.length < 3) {
      this.setData({
        select: false
      })
    }
  },
  //评论框失去焦点
  blur: function() {
    this.setData({
      level: 0,
      placeholderText: this.data.name + '：'
    })
  },
  //提交评论
  submit: function(e) {
    var that = this;
    var str = e.detail.value.content.trim();
    if (str) {
      wx.showLoading({
        title: '正在提交',
        mask: true
      })
      var IMG = that.data.imgurl;
      if (IMG.length > 0) {
        console.log('先上传图片')
        for (var i = 0; i < IMG.length; i++) {
          that.upload(str, IMG[i]);
          console.log(IMG[i])
        }

      } else {
        that.form(str, 1);
      }
    } else {
      api.Toast(2, '请输入你要回复的内容！', that, 1500);
    }
  },
  list: function(tid, page, spid) {
    if (page != 1) {
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
    }

    var that = this;
    wx.request({
      url: baseUrl + 'topic/replies/sub',
      header: {
        'content-type': 'application/json',
        'cld.stats.page_entry': app.globalData.scene,
        'token': app.globalData.token,
        'version': app.globalData.version
      },
      method: 'POST',
      data: {
        page: page,
        tid: parseInt(tid),
        spid: parseInt(spid)
      },
      success: function(res) {
        if (res.statusCode == 200) {
          var Data = that.data.replies.concat(res.data.replies);
          if (Data != null && Data.length > 0) {
            that.setData({
              replies: Data,
              page: page
            })
            if (res.data.replies.length < 10) {
              that.setData({
                all: true,
                more: false
              })
            } else {
              that.setData({
                all: false,
                more: true
              })
            }
          } else {
            that.setData({
              none: true
            })
          }
        } else {
          if (res.data.message != null && res.data.message.length > 0) {
            api.Toast(2, res.data.message, that, 1500);
          } else {
            api.Toast(2, '请求数据失败', that, 1500);
          }
        }
      },
      fail: function() {
        api.Toast(2, '请求数据失败', that, 1500);
        wx.navigateBack({
          delta: 1
        })
      },
      complete: function() {
        that.setData({
          loading: true
        })
        wx.hideLoading();
      }
    })

  },
  upload: function(content, src) {
    var that = this;

    wx.uploadFile({
      url: baseUrl + 'upload/image',
      filePath: src,
      name: 'file',
      header: {
        'Content-Type': 'multipart/form-data',
        'token': app.globalData.token,
        'version': app.globalData.version
      },
      method: 'POST',
      name: '1111.jpg',
      success: res => {
        if (res.statusCode == 200) {
          imgArray.push(JSON.parse(res.data).url);
          console.log('imgArray', imgArray)
          console.log('imgurl', that.data.imgurl)
          if (imgArray.length == that.data.imgurl.length) {
            that.form(content, 2);
          } else {

          }
        } else {
          failArry.push(url);
        }
      },
      fail: function() {
        imgArray = [];
        failArry.push(url);
      }
    })

  },
  form: function(content, type) {
    console.log(this.data.index)
    var that = this;

    var replise = parseInt(that.data.tuid);
    wx.request({
      url: baseUrl + '/topic/reply',
      header: {
        'content-type': 'application/json',
        'cld.stats.page_entry': app.globalData.scene,
        'token': app.globalData.token,
        'version': app.globalData.version
      },
      method: 'POST',
      data: {
        "content": content,
        'media': imgArray,
        "type": type,
        "tuid": that.data.tuid,
        "tid": that.data.tid,
        "pid": that.data.pid,
        'suid': app.globalData.uid,
        'spid': that.data.spid
      },
      success: function(res) {
        if (res.statusCode == 200) {
          var replyCnt = that.data.replyCnt;
          replyCnt++;
          api.Toast(1, '评论成功', that, 1500);
          failArry = [];
          imgArray = [];
          var avatar = app.globalData.userInfo.avatar;
          var Name = app.globalData.userInfo.name;
          var newReply = [{
            'active': '1',
            content: res.data.content,
            ctime: res.data.ctime,
            goodCnt: 0,
            id: res.data.id,
            media: res.data.media,
            pid: res.data.pid,
            replyUser: {
              avatar: avatar,
              id: app.globalData.userInfo.uid,
              name: Name,
            },
            repliesUser: {
              avatar: '',
              id: that.data.tuid,
              name: that.data.name,
            },
            subReplyList: [],
            suid: res.data.tuid,
            tid: res.data.tid,
            type: type,
            zan: 0
          }];
          for (var i = 0; i < that.data.replies.length; i++) {
            newReply.push(that.data.replies[i])
          }
          // debugger
          console.log(newReply)
          that.setData({
            bool: false,
            textarea: '',
            imgurl: [],
            replyCnt: replyCnt,
            select: false,
            dialog: true,
            none: false,
            placeholder: true,
            level: 0,
            replies: newReply,
            none: false
          }, function() {
            // debugger
            console.log(that.data.replies)
            wx.pageScrollTo({
              scrollTop: 0,
              duration: 300
            })

          })
        } else {
          if (res.data.message != null && res.data.message.length > 0) {
            api.Toast(2, res.data.message, that, 1500);
          } else {
            api.Toast(2, '评论失败', that, 1500);
          }
        }

      },
      fail: res => {
        api.Toast(2, '评论失败', that, 1500);
      },
      complete: res => {
        wx.hideLoading();
      }
    })
  },
  level1: function(res) {
    var that = this;
    var data = res;
    for (var i = 0; i < data.length; i++) {
      aryData = [];
      that.level2(data[i].subReplyList.reverse(), data[i].replyUser);
      data[i].subReplyList = aryData;
    }
    console.log(that.data.replies)
    var Data = that.data.replies.concat(data);
    that.setData({
      replies: Data
    }, function() {
      console.log('replies', that.data.replies)
    })
  },

  level2: function(data, replyedUser) {
    var that = this;
    if (data != null && data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        data[i].replyedUser = replyedUser
        aryData = aryData.concat(data[i]);
        that.level2(data[i].subReplyList, data[i].replyUser);
      }
    }
  },
  userinfo: function (e) {
      var that = this;
      var name = e.detail.userInfo.nickName;
      var Avatar = e.detail.userInfo.avatarUrl;
      wx.request({
          url: baseUrl + 'user',
          header: {
              'content-type': 'application/json',
              'token': app.globalData.token
          },
          method: 'PUT',
          data: {
              name: e.detail.userInfo.nickName,
              gender: e.detail.userInfo.gender,
              avatar: Avatar,
              desc: '',
              address: e.detail.userInfo.city
          },
          success: function (res) {
              if (res.statusCode == 200) {
                  wx.request({
                      url: baseUrl + 'user',
                      header: {
                          'content-type': 'application/json',
                          'token': app.globalData.token
                      },
                      method: 'GET',
                      success: function (res) {
                          if (res.statusCode == 200) {
                              app.globalData.auth = true;
                              wx.setStorageSync('auth', 'true');
                              var userInfo = that.data.userInfo;
                              userInfo.avatar = Avatar;
                              app.globalData.userInfo.avatar = Avatar;
                              wx.setStorageSync('userInfo', res.data);
                              app.globalData.userInfo = res.data;
                              app.globalData.uid = res.data.uid;
                              that.setData({
                                  Auth: false,
                                  avatar: Avatar,
                                  userInfo: userInfo,
                              })
                          }
                      },
                      fail: () => {
                          api.Toast(2, '获取数据失败', that, 1500);
                      }
                  })
              } else {
                  api.Toast(2, '获取数据失败', that, 1500);
              }
          },
          fail: function () {
              api.Toast(2, '获取数据失败', that, 1500);
          },
          complete: function () {
              wx.hideLoading();
              var auth = that.data.auth
              auth.hidden = true;
              that.setData({
                  auth: auth
              })
          }
      })
  },
  cancelAuth: function () {
      var that = this;
      var auth = that.data.auth;
      auth.hidden = true;
      that.setData({
          auth: auth
      })
  },
})