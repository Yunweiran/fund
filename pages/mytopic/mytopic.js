// pages/community/community.js
var app = getApp();
var baseUrl = getApp().baseUrl;
var Auth;
var Notice;
import api from '../../utils/util.js'
var ctx;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        Notice: true,
        more: true,
        link: true,
        item: {
            flag: '',
            msg: '',
            toast: true,
        },
        page: 1,
        circle: [],
        all: false,
        more: true,
        Loading: true,
        none: false,
        notice: false,
        dialog: true,
        ava: '',
        auth: {
            hidden: true
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        if (options.uid) {
            wx.setStorageSync('shareuid', options.uid);
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        ctx = wx.createCanvasContext('canvas')
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        console.log("show")
        var that = this;
        Auth = setInterval(() => {
            var auth = wx.getStorageSync('auth');
            console.log("auth", auth)
            if (auth != null && auth.length > 0) {
                clearInterval(Auth);
                var detail = wx.getStorageSync('Detail');
                if (detail) {
                    wx.removeStorageSync('Detail');
                } else {
                    that.setData({
                        circle: [],
                    }, function () {
                        that.topicList(that.data.page, 1);
                    })

                }

            } else {
                setTimeout(() => {
                    clearInterval(Auth);
                }, 10000)
            }
        }, 100);
        Notice = setInterval(() => {
            var notice = wx.getStorageSync('isNotice');
            if (notice.toString()) {
                clearInterval(Notice);
                if (notice != 0) {
                    that.setData({
                        notice: true
                    })
                    wx.setTabBarBadge({
                        index: 2,
                        text: notice.toString()
                    })
                } else {
                    that.setData({
                        notice: false
                    })
                    wx.removeTabBarBadge({
                        index: 2,
                    })
                }

            } else {
                setTimeout(() => {
                    clearInterval(Notice);
                }, 10000)
            }
        }, 100)
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        var detail = wx.getStorageSync('Detail');
        if (detail) {

        } else {
            this.setData({
                circle: []
            })
        }
        clearInterval(Notice);
        clearInterval(Auth);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearInterval(Auth);
        clearInterval(Notice);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        wx.showNavigationBarLoading(); //开启顶部下拉刷新loading
        var that = this;
        this.topicList(1, 0, 0)
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        var that = this;
        var page = this.data.page;
        if (this.data.more) {
            page++;
            that.setData({
                Loading: false
            })
            this.topicList(page, 0, 1);
        }
    },
    onShareAppMessage: function (res) {
        wx.setStorageSync('Detail', true);
        if (res.from === 'button') {
            var pics = '../../img/logo.png';
            var data = res.target.dataset;
            var title = res.target.dataset.item.content
            if (res.target.dataset.item.pics != null && res.target.dataset.item.pics.length > 0) {
                pics = res.target.dataset.item.pics[0]
            }
            return {
                title: title,
                path: '/pages/topic/topic?uid=' + app.globalData.uid + '&scene=' + res.target.dataset.id,
                imageUrl: pics
            }
        } else {
            return {
                title: '邀请好友送999个糖果',
                path: '/pages/index/index?scene=' + app.globalData.uid,
                imageUrl: '../../img/logo.png'
            }
        }

    },
    //评论图片预览
    commentPriview: function (e) {
        var arry = [];
        arry.push(e.currentTarget.dataset.img);
        wx.setStorageSync('Detail', true);
        wx.previewImage({
            urls: arry,
            current: e.currentTarget.dataset.img,
        })
    },
    share: function (e) {
        var id = e.currentTarget.dataset.id;
        console.log(id)
        wx.showShareMenu({
            withShareTicket: true
        })
    },
    detail: function (e) {
        var that = this;
        wx.setStorageSync('Detail', true);
        link(e.currentTarget.dataset.url, that);
    },
    release: function () {
        var that = this;
        link('../release/release', that);
    },
    //圈子封面图预览
    preview: function (e) {
        console.log(e.currentTarget.dataset.src)
        var src = e.currentTarget.dataset.src;
        var list = e.currentTarget.dataset.array;
        console.log(list)
        wx.setStorageSync('Detail', true);
        wx.previewImage({
            urls: list,
            current: src,
        })
    },
    notice: function () {
        var that = this;
        link('../notice/notice', that);
    },
    share: function (e) {
        var that = this;
        if (app.globalData.auth) {
            wx.showLoading({
                title: '正在生成图片中',
                mask: true,
            })
            var data = e.currentTarget.dataset;
            wx.getImageInfo({
                src: data.avatar,
                success: function (res) {
                    that.setData({
                        ava: res.path
                    }, function () {

                        that.code(data);
                    })
                },
                fail: function () { }
            })
        } else {
            var auth = that.data.auth;
            auth.hidden = false
            that.setData({
                auth: auth
            })
        }
    },
    code: function (data) {

        var that = this;
        wx.request({
            url: baseUrl + 'sharecode',
            method: 'POST',
            header: {
                'token': app.globalData.token,
                'version': app.globalData.version
            },
            data: {
                scene: data.id + '&uid=' + app.globalData.uid,
                page: 'pages/community/community',
            },
            success: res => {
                if (res.statusCode == 200) {
                    wx.getImageInfo({
                        src: res.data.url,
                        success: function (res) {
                            console.log(res.path)
                            that.setData({
                                code_img: res.path
                            }, function () {
                                that.draw(res.path, data)
                            })
                        },
                        fail: function () { }
                    })
                } else {
                    api.Toast(2, '获取小程序码失败，请重试', that, 1000);
                }

            },
            fail: function () {
                api.Toast(2, '获取小程序码失败，请重试', that, 1000);
            }
        })
    },
    draw: function (path, data) {
        var that = this
        ctx.setFillStyle('#ffffff');
        ctx.fillRect(0, 0, 270, 360);

        var avatarurl_width = 50; //绘制的头像宽度
        var avatarurl_heigth = 50; //绘制的头像高度
        var avatarurl_x = 20; //绘制的头像在画布上的位置
        var avatarurl_y = 20; //绘制的头像在画布上的位置
        ctx.save();
        ctx.beginPath(); //开始绘制
        ctx.arc(avatarurl_width / 2 + avatarurl_x, avatarurl_heigth / 2 + avatarurl_y, avatarurl_width / 2, 0, Math.PI * 2, false);
        ctx.clip();
        ctx.drawImage(that.data.ava, avatarurl_x, avatarurl_y, avatarurl_width, avatarurl_heigth);
        ctx.restore();
        ctx.setFontSize(16);
        ctx.setFillStyle('#4c84ff');
        ctx.fillText(data.name, 90, 40);
        ctx.setFontSize(14);
        ctx.setFillStyle('#999999');
        ctx.fillText(data.time, 90, 65);


        ctx.setFontSize(12);
        ctx.setFillStyle('#999999');
        ctx.fillText('扫码查看,跟牛人一起学挣钱', 65, 320);

        ctx.setFontSize(12);
        ctx.setFillStyle('#999999');
        ctx.fillText('注册即送200币', 90, 340);

        var title = data.content;
        if (title.length > 30) {
            title = title.substring(0, 30) + '...'
        } else {
            title = title;
        };
        var lineWidth = 0;
        var canvasWidth = 270;
        var lastSubStrIndex = 0;
        var initX = 20;
        var initY = 100;
        var lineHeight = 25;
        ctx.setFontSize(14);
        ctx.setFillStyle('#333333');
        for (let i = 0; i < title.length; i++) {
            var aaaa = title[i]
            var item = ctx.measureText(aaaa);
            lineWidth += item.width;
            if (lineWidth > canvasWidth - initX * 2) {
                ctx.fillText(title.substring(lastSubStrIndex, i), initX, initY);
                initY += lineHeight;
                lineWidth = 0;
                lastSubStrIndex = i;
            }
            if (i == title.length - 1) {
                ctx.fillText(title.substring(lastSubStrIndex, i + 1), initX, initY);
            }
        }
        ctx.drawImage(path, 60, 150, 150, 150);

        that.setData({
            dialog: false
        })
        ctx.draw(false, function () {
            wx.canvasToTempFilePath({
                x: 0,
                y: 0,
                width: 270,
                height: 360,
                destWidth: 270,
                destHeight: 360,
                canvasId: 'canvas',
                success: function (res) {
                    that.setData({
                        imgUrl: res.tempFilePath
                    })
                },
                fail: function (err) {
                    api.Toast(2, '图片生成失败，请重试！', that, 1000);
                },
                complete: function () {
                    wx.hideLoading();
                }
            })

        })
    },
    //保存图片到本地
    saveImg: function () {
        var that = this;
        wx.saveImageToPhotosAlbum({
            filePath: that.data.imgUrl,
            success: function (res) {
                wx.showModal({
                    title: '成功保存图片',
                    content: '已成功为您保存图片到手机相册，请自行前往朋友圈分享!',
                    showCancel: false,
                    confirmText: '知道了',
                    success: function (res) {
                        if (res.confirm) {
                            that.setData({
                                dialog: true
                            })
                        }
                    }
                })
            },
        })
    },
    close: function () {
        this.setData({
            dialog: true
        })
    },
    topicList: function (page, type, flag) {
        var that = this;
        wx.request({
            url: baseUrl + 'topics',
            header: {
                'content-type': 'application/json',
                'cld.stats.page_entry': app.globalData.scene,
                'token': app.globalData.token,
                'version': app.globalData.version,

            },
            method: 'POST',
            data: {
                page: page,
                type: type,
            },
            success: res => {
                if (res.statusCode == 200) {
                    var Data = res.data;
                    if (Data != null && Data.length > 0) {
                        for (var i = 0; i < Data.length; i++) {
                            var media = [];
                            if (Data[i].pics.length > 0) {
                                media = Data[i].pics.split(",");
                            } else {
                                media = null
                            }
                            Data[i].pics = media
                        }
                        if (flag == 0) {
                            that.setData({
                                circle: Data
                            })
                        } else {
                            var Data = that.data.circle.concat(Data);
                            that.setData({
                                circle: Data
                            })
                        }

                        if (res.data.length < 10) {
                            that.setData({
                                all: true,
                                more: false
                            })
                        } else {
                            that.setData({
                                all: false,
                                more: true
                            })
                        }

                    }
                } else {
                    that.setData({
                        more: false,
                        all: false
                    })
                    if (res.data.message != null && res.data.message.length > 0) {
                        api.Toast(2, res.data.message, that, 1500);
                    } else {
                        api.Toast(2, '请求数据失败', that, 1500);
                    }
                }
            },
            fail: res => {
                api.Toast(2, '请求数据失败,请稍后再试', that, 1500);
            },
            complete: res => {
                that.setData({
                    Loading: true
                })
                wx.removeStorageSync('Detail');
                wx.hideNavigationBarLoading();
            }
        })
    },
})

function link(url, that) {
    if (that.data.link == true) {
        that.setData({
            link: false
        }, function () {
            wx.navigateTo({
                url: url,
                complete: function () {
                    that.setData({
                        link: true
                    })
                }
            })
        })
    } else {
        return false;
    }
}