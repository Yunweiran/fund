//app.js
var UID;
var TOKEN;
App({
    baseUrl: 'https://www.bixingqiu.info/api/v1/zt/',
    onLaunch: function(options) {
        
    },
    onShow: function(options) {
        var that = this;
        var token = wx.getStorageSync('token');
        if (token != null && token.length > 0) {
            that.globalData.token = token;
            that.config(token);
        }
        // console.log('app---scene', options.scene)
        this.globalData.scene = options.scene;
        var scene = [1007, 1008, 1011, 1012, 1013, 1012, 1025, 1031, 1032, 1047, 1048, 1049, 1074];
        var indexof = scene.indexOf(options.scene);
        // console.log('app---indexof', indexof)
        if (indexof == -1) {
            if (token != null && token.length > 0) {
                that.globalData.token = token;
                that.auth(token);
            } else {
                this.token();
            }
        } else {
            UID = setInterval(() => {
                var shareuid = wx.getStorageSync('shareuid');
                console.log('app---shareuid', shareuid)
                if (shareuid != null && shareuid.length > 0) {
                    if (token != null && token.length > 0) {
                        that.globalData.token = token;
                        that.auth(token);
                    } else {
                        this.token(shareuid);
                    }

                    clearInterval(UID);
                } else {
                    setTimeout(() => {
                        clearInterval(UID);
                    }, 10000)
                }
            }, 100)
        }

        this.update();
    },
    config: function(token) {
        var that = this;
        wx.request({
            url: that.baseUrl + '/config',
            header: {
                'content-type': 'application/json',
                'token': token,
                'version': that.globalData.version
            },
            method: 'GET',
            success: res => {
                if (res.statusCode == 200) {
                    wx.setStorageSync('config', res.data);

                }
            },
            fail: res => {

            },
            complete: res => {

            }
        })
    },
    update: function() {
        const updateManager = wx.getUpdateManager()
        updateManager.onCheckForUpdate(function(res) {
            // 请求完新版本信息的回调
            //   console.log(res.hasUpdate)
        })
        updateManager.onUpdateReady(function() {
            wx.showModal({
                title: '更新提示',
                content: '发现新版本，是否更新？',
                success: function(res) {
                    if (res.confirm) {
                        updateManager.applyUpdate()
                    }
                }
            })
        })
        updateManager.onUpdateFailed(function() {
            // 新的版本下载失败
            wx.showToast({
                title: '更新失败！',
                icon: 'success',
                duration: 2000
            })
        })
    },
    token: function(shareuid) {
        // console.log('token---shareuid:', shareuid)
        var that = this;
        var header;
        if (shareuid != null && shareuid.length > 0) {
            header = {
                uid: shareuid
            }
        }
        // console.log(shareuid)
        wx.login({
            success: function(res) {
                if (res.code) {
                    wx.request({
                        url: that.baseUrl + 'wx/' + res.code,
                        method: 'GET',
                        header: header,
                        success: function(res) {
                            if (res.statusCode == 200) {
                                that.globalData.token = res.data.token;
                                wx.setStorageSync('token', res.data.token);
                                that.auth(res.data.token);
                                that.config(res.data.token);
                            }
                        },
                        fail: () => {

                        }
                    })
                } else {
                    console.log('登录失败！' + res.errMsg)
                }
            }
        });
    },
    auth: function(token) {
        var that = this;
        wx.request({
            url: that.baseUrl + 'auth/user',
            header: {
                'content-type': 'application/json',
                'token': token
            },
            method: 'GET',
            success: function(res) {
                if (res.statusCode == 200) {
                    if (res.data.auth) {
                        that.userInfo(token);
                        that.globalData.auth = true;
                        wx.setStorageSync('auth', 'true');
                        that.notice();

                    } else {
                        that.globalData.auth = false;
                        wx.setStorageSync('auth', 'false');
                    }
                }
            },
            fail: () => {

            }
        })
    },
    userInfo: function(token) {
        var that = this;
        wx.request({
            url: that.baseUrl + 'user',
            header: {
                'content-type': 'application/json',
                'token': token
            },
            method: 'GET',
            success: function(res) {
                if (res.statusCode == 200) {
                    wx.setStorageSync('userInfo', res.data);
                    that.globalData.userInfo = res.data;
                    that.globalData.uid = res.data.uid
                }
            },
            fail: () => {

            }
        })
    },
    login: function() {

    },
    //通知
    notice: function() {
        var that = this;
        wx.request({
            url: that.baseUrl + 'message/topic/notice',
            header: {
                'content-type': 'application/json',
                'token': that.globalData.token,
                'version': that.globalData.version
            },
            method: 'GET',
            success: res => {
                if (res.statusCode == 200) {
                    that.globalData.isNotice = res.data.noticeNum;
                    wx.setStorageSync('isNotice', res.data.noticeNum);
                    if (res.data.isNotice != 0) {
                        wx.setTabBarBadge({
                            index: 2,
                            text: res.data.noticeNum.toString()
                        })
                    } else {
                        wx.removeTabBarBadge({
                            index: 2,
                        })
                    }
                }
            },
            fail: res => {

            },
            complete: res => {

            }
        })
    },


    globalData: {
        token: '',
        uid: '',
        userInfo: '',
        auth: '',
        isNotice: '',
        shareuid: '',
        version: '1.1.4',
    }
})