var app = getApp();
var Toast = (flag, msg, that, time) => {
  that.data.item.flag = flag;
  that.data.item.msg = msg;
  that.data.item.toast = false;
  that.setData({
    item: that.data.item,
  })
  setTimeout(function () {
    that.data.item.toast = true;
    that.setData({
      item: that.data.item,
    })//1.5秒之后弹窗隐藏
  }, time)
}
//判断授权
var authorize = function (msg) {
    wx.showModal({
        title: '温馨提示',
        content: msg,
        success: function (res) {
            if (res.confirm) {
                //点击确定打开授权页面
                wx.openSetting({
                    success: (res) => {
                        console.log(res)
                        if (res.authSetting['scope.userInfo']) {
                            //已经授权则获取用户微信数据之后提交到后台
                            
                            app.login();
                        } else {
                            // 拒绝授权设置授权为false
                            app.globalData.Authorize = false;
                            app.globalData.scene = 1001;//重设场景值，详情页不再显示回到首页
                            wx.setStorage({//将授权状态缓存到本地
                                key: 'Authorize',
                                data: 'false',
                            })
                        }
                    }
                })
            }
        }
    })
}
module.exports = {
  Toast: Toast,
  authorize: authorize,
}